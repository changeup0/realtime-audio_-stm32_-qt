## 采集
1. 确认ADC采集方案可行                              (噪音比较大)
    - arduino测试模块                               OK
    - stm32测试采样频率可达40k以上                   OK
    - 测试定时时间62.5us是否可行                     OK
    - 使用定时器+ADC实现音频采集                     OK
    - 使用DMA定时搬运                               OK
2. 用WM8978模块实现音频采集
    - IIC接口测试
    - IIS接口测试
    - 实现IIS+DMA数据采集
## 传输
1. Wifi无线传输测试(UART or SPI    需要考虑带宽)
    - 测试8266模块正常工作                          OK
    - 用8266实现数据传输                            OK
## 接收
1. UDP调试助手                                      数据存在丢包问题(10%-15%)
2. Qt                                               QUdpSocket在windows环境下丢包严重
```
引脚分配：
    IIS：
        C6  MCK
        B12 WS
        B13 CK
        B15 SD
    IIC:
        B10 SCL
        B11 SDA
    Uart1：
        A8  Rx
        A9  Tx
    Uart2：
        A2  Tx
        A3  Rx
    DMA:
        IIS DMA1 Ch4
        UART2 DMA2  Ch7
```