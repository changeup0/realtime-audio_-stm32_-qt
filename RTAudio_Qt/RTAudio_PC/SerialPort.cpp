#include "SerialPort.h"
// #include "widget.h"
// #include "graphics.h"
#include <QTextEdit>
#include <QTextBrowser>
#include <QTableWidget>
#include <QLabel>
#include <QComboBox>
#include <QStringList>
#include <QString>
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QLineEdit>
#include <QPushButton>
#include <QStatusBar>
#include <QCheckBox>
#include <QDebug>

SerialPort::SerialPort(QWidget *parent)
    : QMainWindow(parent)
{

    //窗口大小设定
    this->setWindowTitle(tr("串口调试"));
    this->resize(700, 450);
    this->setMinimumSize(700, 450);
    this->setMaximumSize(700, 450);

    Init_Interface();
    Init_StatusBar();
    this->CntReset();
    connect(openButton, &QPushButton::clicked, this, &SerialPort::OpenSerial);
    connect(cntResetBtn, &QPushButton::clicked, this, &SerialPort::CntReset);
    connect(clearBtn, &QPushButton::clicked, this, &SerialPort::ClearWindow);
    connect(sendBtn, &QPushButton::clicked, this, &SerialPort::SendText);

    connect(cmd0SendBtn, &QPushButton::clicked, this, [=]() { this->Trans_Protocol(this->cmd0LineEdit->text()); });
    connect(cmd1SendBtn, &QPushButton::clicked, this, [=]() { this->Trans_Protocol(this->cmd1LineEdit->text()); });
    connect(cmd2SendBtn, &QPushButton::clicked, this, [=]() { this->Trans_Protocol(this->cmd2LineEdit->text()); });
    connect(cmd3SendBtn, &QPushButton::clicked, this, [=]() { this->Trans_Protocol(this->cmd3LineEdit->text()); });
    connect(cmd4SendBtn, &QPushButton::clicked, this, [=]() { this->Trans_Protocol(this->cmd4LineEdit->text()); });
    connect(cmd5SendBtn, &QPushButton::clicked, this, [=]() { this->Trans_Protocol(this->cmd5LineEdit->text()); });
    connect(cmd6SendBtn, &QPushButton::clicked, this, [=]() { this->Trans_Protocol(this->cmd6LineEdit->text()); });
    connect(cmd7SendBtn, &QPushButton::clicked, this, [=]() { this->Trans_Protocol(this->cmd7LineEdit->text()); });

    connect(tcpHelperBtn, &QPushButton::clicked, this, &SerialPort::TcpHelper);
    connect(graphicBtn, &QPushButton::clicked, this, &SerialPort::GraphicWin);
}

SerialPort::~SerialPort()
{
    if(this->openButton->text() == "打开")
        delete mySerialPort;
    Destroy_StatusBar();
    Destroy_Interface();
}

void SerialPort::Trans_Protocol(QString tmpStr)
{
    if (this->protocolSendChkBox->isChecked())
    {
        tmpStr = QString("%1%2%3").arg(this->frameHeadEdit->text()).arg(tmpStr).arg(this->frameTailEdit->text());
        qDebug() << tmpStr;
    }
    sendCnt += tmpStr.toStdString().size();
    this->sendCntLabel->setText(QString::number(sendCnt));
    mySerialPort->write(tmpStr.toLocal8Bit());
}

void SerialPort::ClearWindow()
{
    this->recDataTB->clear();
    sendCnt = 0;
    recCnt = 0;
    this->recCntLabel->setText(QString::number(recCnt));
    this->sendCntLabel->setText(QString::number(sendCnt));
}

void SerialPort::CntReset()
{
    sendCnt = 0;
    recCnt = 0;
    this->recCntLabel->setText(QString::number(recCnt));
    this->sendCntLabel->setText(QString::number(sendCnt));
}

void SerialPort::OpenSerial()
{
    if (this->openButton->text() == "打开")
    {
        mySerialPort = new QSerialPort;
        mySerialPort->setPortName(this->comComboBox->currentText());
        mySerialPort->open(QIODevice::ReadWrite);
        switch (this->baudComboBox->currentIndex())
        {
        case 0:
            mySerialPort->setBaudRate(QSerialPort::Baud9600);
            break;
        case 1:
            mySerialPort->setBaudRate(QSerialPort::Baud38400);
            break;
        case 2:
            mySerialPort->setBaudRate(QSerialPort::Baud115200);
            break;
        default:
            mySerialPort->setBaudRate(QSerialPort::Baud9600);
            break;
        }
        // DataBits
        switch (this->dataBitsComboBox->currentIndex())
        {
        case 0:
            mySerialPort->setDataBits(QSerialPort::Data8);
            break;
        default:
            break;
        }

        switch (this->cmpComboBox->currentIndex())
        {
        case 0:
            mySerialPort->setParity(QSerialPort::NoParity);
            break;
        default:
            break;
        }

        switch (this->stopBitsComboBox->currentIndex())
        {
        case 0:
            mySerialPort->setStopBits(QSerialPort::OneStop);
            break;
        case 1:
            mySerialPort->setStopBits(QSerialPort::TwoStop);
            break;
        default:
            break;
        }

        mySerialPort->setFlowControl(QSerialPort::NoFlowControl);

        this->comComboBox->setEnabled(false);
        this->baudComboBox->setEnabled(false);
        this->cmpComboBox->setEnabled(false);
        this->stopBitsComboBox->setEnabled(false);
        this->dataBitsComboBox->setEnabled(false);
        this->openButton->setText(tr("断开"));
        this->openButton->setEnabled(true);
        this->sendBtn->setEnabled(true);
        cmd7SendBtn->setEnabled(true);
        cmd6SendBtn->setEnabled(true);
        cmd5SendBtn->setEnabled(true);
        cmd4SendBtn->setEnabled(true);
        cmd3SendBtn->setEnabled(true);
        cmd2SendBtn->setEnabled(true);
        cmd1SendBtn->setEnabled(true);
        cmd0SendBtn->setEnabled(true);

        connect(mySerialPort, &QSerialPort::readyRead, this, &SerialPort::SerialReadData);
    }
    else
    {
        mySerialPort->clear();
        mySerialPort->close();
        mySerialPort->deleteLater();
        delete mySerialPort;

        this->comComboBox->setEnabled(true);
        this->baudComboBox->setEnabled(true);
        this->cmpComboBox->setEnabled(true);
        this->stopBitsComboBox->setEnabled(true);
        this->dataBitsComboBox->setEnabled(true);
        this->openButton->setText(tr("打开"));
        this->openButton->setEnabled(true);
        this->sendBtn->setEnabled(false);
        cmd7SendBtn->setEnabled(false);
        cmd6SendBtn->setEnabled(false);
        cmd5SendBtn->setEnabled(false);
        cmd4SendBtn->setEnabled(false);
        cmd3SendBtn->setEnabled(false);
        cmd2SendBtn->setEnabled(false);
        cmd1SendBtn->setEnabled(false);
        cmd0SendBtn->setEnabled(false);
    }
}

void SerialPort::SendText()
{
    this->Trans_Protocol(QString::fromStdString(this->sendDataTE->toPlainText().toStdString()));
}

void SerialPort::SerialReadData()
{
    QByteArray buffer;
    buffer = mySerialPort->readAll();
    if (!buffer.isEmpty())
    {
        QString tmpStr = QString::fromLocal8Bit(buffer);
        if (protocolRecChkBox->isChecked())
        {
            if (tmpStr.startsWith(frameHeadEdit->text()) && tmpStr.endsWith(frameTailEdit->text()))
            {
                tmpStr = tmpStr.mid(frameHeadEdit->text().size(), tmpStr.size() - frameHeadEdit->text().size() - frameTailEdit->text().size());
                this->recDataTB->append(tmpStr);
                recCnt += tmpStr.toStdString().size();
                this->recCntLabel->setText(QString::number(recCnt));
            }
        }
        else
        {
            this->recDataTB->append(tmpStr);
            recCnt += tmpStr.toStdString().size();
            this->recCntLabel->setText(QString::number(recCnt));
        }
        //        if(this->graphicWin != NULL)
        //        {
        //            this->graphicWin->data1 = tmpStr.toDouble();
        //            qDebug() << tmpStr;
        //        }
    }
    buffer.clear();
}

void SerialPort::Init_StatusBar()
{
    bitsCntBar = new QStatusBar(this);
    bitsCntBar->move(150, this->geometry().height() - 40);
    bitsCntBar->resize(400, 40);
    bitsCntBar->setSizeGripEnabled(false);

    recTextLabel = new QLabel(this);
    recTextLabel->setText(tr("已接收字节数:"));
    recTextLabel->resize(100, 20);

    recCntLabel = new QLabel(this);
    recCntLabel->setText("recieve bits");
    recCntLabel->resize(100, 20);

    sendTextLabel = new QLabel(this);
    sendTextLabel->setText(tr("已发送字节数:"));
    sendTextLabel->resize(100, 20);
    sendCntLabel = new QLabel(this);
    sendCntLabel->setText("recieve bits");
    sendCntLabel->resize(100, 20);

    bitsCntBar->addWidget(recTextLabel);
    bitsCntBar->addWidget(recCntLabel);
    bitsCntBar->addPermanentWidget(sendTextLabel);
    bitsCntBar->addPermanentWidget(sendCntLabel);
}
void SerialPort::Init_Interface()
{
    //    quint16 widgetWid = 700;
    //    quint16 widgetHigh = 450;
    quint16 textRecBoxWid = 400;
    quint16 textRecBoxHigh = 250;
    quint16 textSendBoxWid = textRecBoxWid;
    quint16 textSendBoxHigh = 100;
    quint16 labelWid = 50, labelHigh = 20;
    quint16 comboBoxWid = 80, comboBoxHigh = labelHigh;
    quint16 buttonWid = comboBoxWid + labelWid, buttonHigh = 40;
    quint16 cmdLineEditWid = 60, cmdLineEditHigh = labelHigh;
    quint16 cmdSendWid = labelWid, cmdSendHigh = labelHigh;
    //位置调整
    quint16 topGap = 20;
    quint16 leftGap = 10;
    quint16 textBoxComboBoxGap = 10;
    quint16 textBoxLeftGap = leftGap + labelWid + comboBoxWid + textBoxComboBoxGap;
    quint16 bothBoxGap = 30;
    quint16 labelGap = 10;
    quint16 comboBoxGap = labelGap;
    quint16 cmdLineEditTextBoxGap = 10;
    quint16 cmdLineEditLeftGap = textBoxLeftGap + textRecBoxWid + cmdLineEditTextBoxGap;
    quint16 cmdSendBtnLeftGap = cmdLineEditLeftGap + cmdLineEditWid + 10;
    quint16 lineEditGap = 10;

    //接收文本框设定
    recDataTB = new QTextBrowser(this);
    recDataTB->resize(textRecBoxWid, textRecBoxHigh);
    recDataTB->move(textBoxLeftGap, topGap);

    //发送文本框设定
    sendDataTE = new QTextEdit(this);
    sendDataTE->resize(textSendBoxWid, textSendBoxHigh);
    sendDataTE->move(textBoxLeftGap, topGap + textRecBoxHigh + bothBoxGap);

    //波特率标签及选项
    baudLabel = new QLabel(this);
    baudLabel->setText(tr("波特率:"));
    baudLabel->resize(labelWid, labelHigh);
    baudLabel->move(leftGap, topGap);

    QStringList tmpList;
    tmpList << "9600"
            << "38400"
            << "115200";
    baudComboBox = new QComboBox(this);
    baudComboBox->addItems(tmpList);
    baudComboBox->resize(comboBoxWid, comboBoxHigh);
    baudComboBox->move(leftGap + labelWid, topGap);

    //数据位标签及选项设定
    dataBitsLabel = new QLabel(this);
    dataBitsLabel->setText(tr("数据位:"));
    dataBitsLabel->resize(labelWid, labelHigh);
    dataBitsLabel->move(leftGap, topGap + labelHigh + labelGap);

    tmpList.clear();
    tmpList << "8";
    dataBitsComboBox = new QComboBox(this);
    dataBitsComboBox->addItems(tmpList);
    dataBitsComboBox->resize(comboBoxWid, comboBoxHigh);
    dataBitsComboBox->move(leftGap + labelWid, topGap + comboBoxHigh + comboBoxGap);

    //停止位标签及选项设定
    stopBitsLabel = new QLabel(this);
    stopBitsLabel->setText(tr("停止位："));
    stopBitsLabel->resize(labelWid, labelHigh);
    stopBitsLabel->move(leftGap, dataBitsLabel->y() + labelHigh + labelGap);

    tmpList.clear();
    tmpList << "1";
    stopBitsComboBox = new QComboBox(this);
    stopBitsComboBox->addItems(tmpList);
    stopBitsComboBox->resize(comboBoxWid, comboBoxHigh);
    stopBitsComboBox->move(leftGap + labelWid, dataBitsComboBox->y() + comboBoxHigh + comboBoxGap);

    //校验位标签及选项设定
    cmpBitsLabel = new QLabel(this);
    cmpBitsLabel->setText(tr("校验位"));
    cmpBitsLabel->resize(labelWid, labelHigh);
    cmpBitsLabel->move(leftGap, stopBitsLabel->y() + labelHigh + labelGap);

    tmpList.clear();
    tmpList << "0";
    cmpComboBox = new QComboBox(this);
    cmpComboBox->addItems(tmpList);
    cmpComboBox->resize(comboBoxWid, comboBoxHigh);
    cmpComboBox->move(leftGap + labelWid, stopBitsComboBox->y() + comboBoxHigh + comboBoxGap);

    //端口号标签及选项设定
    comLabel = new QLabel(this);
    comLabel->setText(tr("端口号"));
    comLabel->resize(labelWid, labelHigh);
    comLabel->move(leftGap, cmpBitsLabel->y() + labelHigh + labelGap);

    comComboBox = new QComboBox(this);
    foreach (const QSerialPortInfo &comInfo, QSerialPortInfo::availablePorts())
    {
        QSerialPort tmpSerial;
        tmpSerial.setPort(comInfo);
        if (tmpSerial.open(QIODevice::ReadWrite))
        {
            comComboBox->addItem(tmpSerial.portName());
            tmpSerial.close();
        }
    }
    comComboBox->resize(comboBoxWid, comboBoxHigh);
    comComboBox->move(leftGap + labelWid, cmpComboBox->y() + comboBoxHigh + comboBoxGap);

    //连接按键设定
    openButton = new QPushButton(this);
    openButton->setText(tr("打开"));
    openButton->resize(buttonWid, buttonHigh);
    openButton->setEnabled(true);
    openButton->move(leftGap, comComboBox->y() + comboBoxHigh + comboBoxGap);

    //帧头帧尾设置
    frameHeadLabel = new QLabel(this);
    frameHeadLabel->setText(tr("帧头:"));
    frameHeadLabel->resize(labelWid, labelHigh);
    frameHeadLabel->move(leftGap, topGap + textRecBoxHigh + bothBoxGap);

    frameHeadEdit = new QLineEdit(this);
    frameHeadEdit->resize(comboBoxWid, comboBoxHigh);
    frameHeadEdit->setText(tr("7M"));
    frameHeadEdit->move(leftGap + labelWid, topGap + textRecBoxHigh + bothBoxGap);

    frameTailLabel = new QLabel(this);
    frameTailLabel->setText(tr("帧尾:"));
    frameTailLabel->resize(labelWid, labelHigh);
    frameTailLabel->move(leftGap, topGap + textRecBoxHigh + bothBoxGap + labelGap + labelHigh);

    frameTailEdit = new QLineEdit(this);
    frameTailEdit->resize(comboBoxWid, comboBoxHigh);
    frameTailEdit->setText(tr("\n"));
    frameTailEdit->move(leftGap + labelWid, topGap + textRecBoxHigh + bothBoxGap + labelGap + labelHigh);

    //指令栏设定
    //指令0
    cmd0LineEdit = new QLineEdit(this);
    cmd0LineEdit->setText(tr("?"));
    cmd0LineEdit->resize(cmdLineEditWid, cmdLineEditHigh);
    cmd0LineEdit->move(cmdLineEditLeftGap, topGap);

    cmd0SendBtn = new QPushButton(this);
    cmd0SendBtn->setText("发送");
    cmd0SendBtn->resize(cmdSendWid, cmdSendHigh);
    cmd0SendBtn->setEnabled(false);
    cmd0SendBtn->move(cmdSendBtnLeftGap, topGap);
    //指令1
    cmd1LineEdit = new QLineEdit(this);
    cmd1LineEdit->setText(tr("+ 1"));
    cmd1LineEdit->resize(cmdLineEditWid, cmdLineEditHigh);
    cmd1LineEdit->move(cmdLineEditLeftGap, topGap + cmdSendHigh + lineEditGap);

    cmd1SendBtn = new QPushButton(this);
    cmd1SendBtn->setText("发送");
    cmd1SendBtn->resize(cmdSendWid, cmdSendHigh);
    cmd1SendBtn->setEnabled(false);
    cmd1SendBtn->move(cmdSendBtnLeftGap, topGap + cmdSendHigh + lineEditGap);
    //指令2
    cmd2LineEdit = new QLineEdit(this);
    cmd2LineEdit->setText(tr("- 1"));
    cmd2LineEdit->resize(cmdLineEditWid, cmdLineEditHigh);
    cmd2LineEdit->move(cmdLineEditLeftGap, cmd1SendBtn->y() + cmdSendHigh + lineEditGap);

    cmd2SendBtn = new QPushButton(this);
    cmd2SendBtn->setText("发送");
    cmd2SendBtn->resize(cmdSendWid, cmdSendHigh);
    cmd2SendBtn->setEnabled(false);
    cmd2SendBtn->move(cmdSendBtnLeftGap, cmd1SendBtn->y() + cmdSendHigh + lineEditGap);

    //指令3
    cmd3LineEdit = new QLineEdit(this);
    cmd3LineEdit->setText(tr("* 0101"));
    cmd3LineEdit->resize(cmdLineEditWid, cmdLineEditHigh);
    cmd3LineEdit->move(cmdLineEditLeftGap, cmd2SendBtn->y() + cmdSendHigh + lineEditGap);

    cmd3SendBtn = new QPushButton(this);
    cmd3SendBtn->setText("发送");
    cmd3SendBtn->resize(cmdSendWid, cmdSendHigh);
    cmd3SendBtn->setEnabled(false);
    cmd3SendBtn->move(cmdSendBtnLeftGap, cmd2SendBtn->y() + cmdSendHigh + lineEditGap);

    //指令4
    cmd4LineEdit = new QLineEdit(this);
    cmd4LineEdit->setText(tr("LM 1"));
    cmd4LineEdit->resize(cmdLineEditWid, cmdLineEditHigh);
    cmd4LineEdit->move(cmdLineEditLeftGap, cmd3SendBtn->y() + cmdSendHigh + lineEditGap);

    cmd4SendBtn = new QPushButton(this);
    cmd4SendBtn->setText("发送");
    cmd4SendBtn->resize(cmdSendWid, cmdSendHigh);
    cmd4SendBtn->setEnabled(false);
    cmd4SendBtn->move(cmdSendBtnLeftGap, cmd3SendBtn->y() + cmdSendHigh + lineEditGap);

    //指令5
    cmd5LineEdit = new QLineEdit(this);
    cmd5LineEdit->setText(tr("LS 6"));
    cmd5LineEdit->resize(cmdLineEditWid, cmdLineEditHigh);
    cmd5LineEdit->move(cmdLineEditLeftGap, cmd4SendBtn->y() + cmdSendHigh + lineEditGap);

    cmd5SendBtn = new QPushButton(this);
    cmd5SendBtn->setText("发送");
    cmd5SendBtn->resize(cmdSendWid, cmdSendHigh);
    cmd5SendBtn->setEnabled(false);
    cmd5SendBtn->move(cmdSendBtnLeftGap, cmd4SendBtn->y() + cmdSendHigh + lineEditGap);

    //指令6
    cmd6LineEdit = new QLineEdit(this);
    cmd6LineEdit->setText(tr("LD 7"));
    cmd6LineEdit->resize(cmdLineEditWid, cmdLineEditHigh);
    cmd6LineEdit->move(cmdLineEditLeftGap, cmd5SendBtn->y() + cmdSendHigh + lineEditGap);

    cmd6SendBtn = new QPushButton(this);
    cmd6SendBtn->setText("发送");
    cmd6SendBtn->resize(cmdSendWid, cmdSendHigh);
    cmd6SendBtn->setEnabled(false);
    cmd6SendBtn->move(cmdSendBtnLeftGap, cmd5SendBtn->y() + cmdSendHigh + lineEditGap);

    //指令7
    cmd7LineEdit = new QLineEdit(this);
    cmd7LineEdit->setText(tr("指令7"));
    cmd7LineEdit->resize(cmdLineEditWid, cmdLineEditHigh);
    cmd7LineEdit->move(cmdLineEditLeftGap, cmd6SendBtn->y() + cmdSendHigh + lineEditGap);

    cmd7SendBtn = new QPushButton(this);
    cmd7SendBtn->setText("发送");
    cmd7SendBtn->resize(cmdSendWid, cmdSendHigh);
    cmd7SendBtn->setEnabled(false);
    cmd7SendBtn->move(cmdSendBtnLeftGap, cmd6SendBtn->y() + cmdSendHigh + lineEditGap);

    sendBtn = new QPushButton(this);
    sendBtn->setText(tr("发送"));
    sendBtn->resize(buttonWid, textSendBoxHigh);
    sendBtn->setEnabled(false);
    sendBtn->move(textBoxLeftGap + textSendBoxWid + 10, topGap + textRecBoxHigh + bothBoxGap);

    cntResetBtn = new QPushButton(this);
    cntResetBtn->setText(tr("计数清零"));
    cntResetBtn->resize(buttonWid, 40);
    cntResetBtn->move(textBoxLeftGap + textSendBoxWid + 10, topGap + textRecBoxHigh + bothBoxGap + textSendBoxHigh + 7);

    clearBtn = new QPushButton(this);
    clearBtn->setText(tr("显示清除"));
    clearBtn->resize(buttonWid, buttonHigh);
    clearBtn->move(leftGap, topGap + 5 * comboBoxHigh + 5 * comboBoxGap + buttonHigh + 10);

    protocolSendChkBox = new QCheckBox(this);
    protocolSendChkBox->move(leftGap, topGap + textRecBoxHigh + bothBoxGap + 2 * labelGap + 2 * labelHigh);
    protocolSendLabel = new QLabel(this);
    protocolSendLabel->resize(labelWid, labelHigh);
    protocolSendLabel->setText(tr("协议发送"));
    protocolSendLabel->move(leftGap + 20, topGap + textRecBoxHigh + bothBoxGap + 2 * labelGap + 2 * labelHigh + 5);

    protocolRecChkBox = new QCheckBox(this);
    protocolRecChkBox->move(leftGap, topGap + textRecBoxHigh + bothBoxGap + 3 * labelGap + 3 * labelHigh);
    protocolRecLabel = new QLabel(this);
    protocolRecLabel->resize(labelWid, labelHigh);
    protocolRecLabel->setText(tr("协议接收"));
    protocolRecLabel->move(leftGap + 20, topGap + textRecBoxHigh + bothBoxGap + 3 * labelGap + 3 * labelHigh + 5);

    tcpHelperBtn = new QPushButton(this);
    tcpHelperBtn->resize(labelWid, labelHigh);
    tcpHelperBtn->setText(tr("TCP助手"));
    tcpHelperBtn->move(leftGap + labelWid + 30, topGap + textRecBoxHigh + bothBoxGap + 2 * labelGap + 2 * labelHigh + 5);

    graphicBtn = new QPushButton(this);
    graphicBtn->resize(labelWid, labelHigh);
    graphicBtn->setText(tr("波形图"));
    graphicBtn->move(leftGap + labelWid + 30, topGap + textRecBoxHigh + bothBoxGap + 3 * labelGap + 3 * labelHigh + 5);
}

void SerialPort::TcpHelper()
{
    //    tcpHelperAssist = new Widget();
    //    tcpHelperAssist->show();
}

void SerialPort::GraphicWin()
{
    //    graphicWin = new graphics();
    //    graphicWin->pTimerUpdate->start(1000);//1s更新一次
    //    graphicWin->startPauseBtn->setText(tr("暂停"));
    //    graphicWin->show();
}

void SerialPort::Destroy_Interface()
{
    delete (recDataTB);
    delete (sendDataTE);
    delete (baudLabel);
    delete (baudComboBox);
    delete (dataBitsLabel);
    delete (dataBitsComboBox);
    delete (stopBitsLabel);
    delete (stopBitsComboBox);
    delete (cmpBitsLabel);
    delete (cmpComboBox);
    delete (comLabel);
    delete (comComboBox);
    delete (openButton);
    delete (frameHeadLabel);
    delete (frameHeadEdit);
    delete (frameTailLabel);
    delete (cmd0LineEdit);
    delete (cmd1LineEdit);
    delete (cmd2LineEdit);
    delete (cmd3LineEdit);
    delete (cmd4LineEdit);
    delete (cmd5LineEdit);
    delete (cmd6LineEdit);
    delete (cmd7LineEdit);
    delete (cmd0SendBtn);
    delete (cmd1SendBtn);
    delete (cmd2SendBtn);
    delete (cmd3SendBtn);
    delete (cmd4SendBtn);
    delete (cmd5SendBtn);
    delete (cmd6SendBtn);
    delete (cmd7SendBtn);
    delete (sendBtn);
    delete (cntResetBtn);
    delete (clearBtn);
    delete (protocolSendChkBox);
    delete (protocolSendLabel);
    delete (protocolRecChkBox);
    delete (protocolSendLabel);
    delete (tcpHelperBtn);
    delete (graphicBtn);
}

void SerialPort::Destroy_StatusBar()
{
    delete (bitsCntBar);
    delete (recTextLabel);
    delete (recCntLabel);
    delete (sendTextLabel);
    delete (sendCntLabel);
    return;
}
