#ifndef SERIALPORT_H
#define SERIALPORT_H

#include <QMainWindow>
#include <QSerialPort>
#include <QTextEdit>
#include <QTextBrowser>
#include <QTableWidget>
#include <QLabel>
#include <QComboBox>
#include <QStringList>
#include <QString>
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QLineEdit>
#include <QPushButton>
#include <QStatusBar>
#include <QCheckBox>
#include <QDebug>


class SerialPort : public QMainWindow
{
    Q_OBJECT

public:
    SerialPort(QWidget *parent = nullptr);
    ~SerialPort();

    void Init_Interface(void);
    void Init_StatusBar(void);
    void Trans_Protocol(QString tmpStr);
    void Destroy_Interface(void);
    void Destroy_StatusBar(void);

private slots:
    void OpenSerial(void);
    void SendText(void);
    void CntReset(void);
    void SerialReadData(void);
    void ClearWindow(void);
    void TcpHelper(void);
    void GraphicWin(void);

private:
    quint32 recCnt, sendCnt;
	//工具栏变量
    QStatusBar * bitsCntBar;
    QLabel * recTextLabel;
    QLabel * recCntLabel;
    QLabel * sendTextLabel;
    QLabel * sendCntLabel;
    //串口
    QSerialPort * mySerialPort;
    //界面变量
    QTextBrowser * recDataTB;
    QTextEdit * sendDataTE;
    QLabel * baudLabel;
    QComboBox * baudComboBox;
    QLabel * dataBitsLabel;
    QComboBox * dataBitsComboBox;
    QLabel * stopBitsLabel;
    QComboBox * stopBitsComboBox;
    QLabel * cmpBitsLabel;
    QComboBox * cmpComboBox;
    QLabel * comLabel;
    QComboBox * comComboBox;
    QPushButton * openButton;
    QLabel * frameHeadLabel;
    QLineEdit * frameHeadEdit;
    QLabel * frameTailLabel;
    QLineEdit * frameTailEdit;
    // 命令栏
    QLineEdit * cmd0LineEdit;
    QPushButton * cmd0SendBtn;
    QLineEdit * cmd1LineEdit;
    QPushButton * cmd1SendBtn;
    QLineEdit * cmd2LineEdit;
    QPushButton * cmd2SendBtn;
    QLineEdit * cmd3LineEdit;
    QPushButton * cmd3SendBtn;
    QLineEdit * cmd4LineEdit;
    QPushButton * cmd4SendBtn;
    QLineEdit * cmd5LineEdit;
    QPushButton * cmd5SendBtn;
    QLineEdit * cmd6LineEdit;
    QPushButton * cmd6SendBtn;
    QLineEdit * cmd7LineEdit;
    QPushButton * cmd7SendBtn;
    // 发送及清零按钮
    QPushButton * sendBtn;
    QPushButton * cntResetBtn;
    QPushButton * clearBtn;
    // 是否采用协议的选择框
    QCheckBox * protocolSendChkBox;
    QLabel * protocolSendLabel;
    QCheckBox * protocolRecChkBox;
    QLabel * protocolRecLabel;
    // 发送按钮
    QPushButton * graphicBtn;

//    graphics * graphicWin;
    bool graphicFlag = false;
    // TCP助手
    QPushButton * tcpHelperBtn;
//    Widget * tcpHelperAssist;


};

#endif // SERIALPORT_H
