
#ifndef UDPHELPER_H
#define UDPHELPER_H

#include <QMainWindow>
#include <QPushButton>
#include <QTextEdit>
#include <QTextBrowser>
#include <QComboBox>
#include <QUdpSocket>
#include <qlistwidget.h>
#include <QString>
#include <QLabel>
#include <QLineEdit>
#include <QDesktopServices>
#include <QUrl>
#include <QMessageBox>

class UDPHelper : public QMainWindow
{
    Q_OBJECT

public:
    UDPHelper(QWidget *parent = 0);//构造函数
    ~UDPHelper();//析构函数

    QUdpSocket * recSocket;
    QUdpSocket * sendSocket;

private slots:
    void startListen(void);//开始监听函数
    virtual void readData(void);//


private:
    void initUi(void);
    void sendAckUDP(QHostAddress ipAddr, quint16 port);
    void destroyUI(void);

    QPushButton * sendBtnPtr;
    QPushButton * listenBtnPtr;
    QPushButton * clearClientButton;
    QPushButton * clearRecTextButton;
    // QPushButton * openLinkBtn;

    QLabel * portLabelPtr;
    QLabel * sendTextlabelPtr;
    QLabel * recTextLabelPtr;
    QLabel * clientListLabelPtr;


    QLineEdit * portLineEditPtr;
    QTextBrowser * recTextBrowerPtr;
    QTextEdit * sendTextEditPtr;
    QListWidget * recListWidgetPtr;
    QListWidget * clintListWidetPtr;

    QComboBox * clienListComBoxPtr;

    const QString ackStr="UDP_Recieved\r\n";

    quint16 serverPort;
};

#endif // UDPHELPER_H
