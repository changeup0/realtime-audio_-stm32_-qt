#include <QApplication>
#include <QLineEdit>
#include <QTextBrowser>

#include "SerialPort.h"
#include "TCPHelper.h"
#include "UDPHelper.h"
#include "UDP_RTAudio.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    // SerialPort w;   //串口
    // w.show();

    // TCPHelper t;    // TCP
    // t.show();

//     UDPHelper u;        // UDP
//     u.show();

    UDP_RTAudio ur;
    ur.show();
    return a.exec();
}
