#ifndef TCPHelper_H
#define TCPHelper_H

#include <QWidget>
#include <QTcpSocket>
#include <QPushButton>
#include <QTextEdit>
#include <QTextBrowser>
#include <QLabel>
#include <QLineEdit>

class TCPHelper : public QWidget
{
    Q_OBJECT

public:
    explicit TCPHelper(QWidget *parent = nullptr);
    ~TCPHelper();


private slots:
    void setUI(void);
    void destroyUI(void);

    void socketRead(void);
    void socketDisconnected(void);
    void buildConnect();

    void sendData();

    void clear();

private:
    QTcpSocket *clientSocket;
    int recCnt, sendCnt;

    // button
    QPushButton *sendBtn, *linkBtn, *clearBtn;
    // label
    QLabel *ipLabel, *sendLabel, *recLabel;
    QLabel *sendCntLCD, *recCntLCD;
    // TextEdit
    QTextEdit *sendText;
    // LineEdit
    QLineEdit *serverIP, *serverPort;
    // TextBrowser
    QTextBrowser *recText;
};

#endif // TCPHelper_H
