#include <QDebug>
#include <QTcpServer>
#include <QTcpSocket>
#include <QCheckBox>

#include "TCPHelper.h"

TCPHelper::TCPHelper(QWidget *parent) : QWidget(parent)
{
    setUI(); 

    clientSocket = new QTcpSocket();
    connect(clientSocket, &QTcpSocket::readyRead, this, &TCPHelper::socketRead);
    connect(clientSocket, &QTcpSocket::disconnected, this, &TCPHelper::socketDisconnected);

    connect(linkBtn, &QPushButton::clicked, this, &TCPHelper::buildConnect);
    connect(clearBtn, &QPushButton::clicked, this, &TCPHelper::clear);
    // ui->sendCntLCD->setText(QString::number(sendCnt));
    // ui->recCntLCD->setText(QString::number(recCnt));
}

TCPHelper::~TCPHelper()
{
    delete this->clientSocket;
    destroyUI();
    // delete ui;
}

void TCPHelper::buildConnect()
{
    if (linkBtn->text() == tr("连接"))
    {
        clientSocket->abort();
        clientSocket->connectToHost(serverIP->text(), static_cast<quint16>(serverPort->text().toInt()));
        if (!clientSocket->waitForConnected(3000))
        {
            qDebug() << tr("连接失败");
            return;
        }
        qDebug() << tr("连接成功");
        linkBtn->setText(tr("连接断开"));
        sendBtn->setEnabled(true);
    }
    else
    {
        clientSocket->disconnectFromHost();
        linkBtn->setText(tr("连接"));
        sendBtn->setEnabled(false);
        qDebug() << tr("连接断开");
    }
}

void TCPHelper::socketDisconnected(void)
{
    linkBtn->setText(tr("连接"));
    sendBtn->setEnabled(false);
    qDebug() << tr("断开连接");
}

void TCPHelper::socketRead(void)
{
    QByteArray buffer;
    buffer = clientSocket->readAll();
    if (!buffer.isEmpty())
    {
        qDebug() << tr("接受字节流数量:") << buffer.size() << endl;
        // QString tmpStr = QString::fromLocal8Bit(buffer);
        // rectext->append(tmpstr);
        // recCnt += tmpStr.toStdString().size();
        // recCntLCD->setText(QString::number(recCnt));
    }
    buffer.clear();
}

void TCPHelper::sendData()
{
    qDebug() << "Send: " << sendText->toPlainText();
    clientSocket->write(sendText->toPlainText().toLocal8Bit());
    sendCnt += sendText->toPlainText().toStdString().size();
    sendCntLCD->setText(QString::number(sendCnt));
    clientSocket->flush();
}

void TCPHelper::clear()
{
    recText->clear();
    sendText->clear();
    sendCnt = 0;
    recCnt = 0;
    recCntLCD->setText(QString::number(recCnt));
    sendCntLCD->setText(QString::number(sendCnt));
}

void TCPHelper::setUI()
{
    this->setWindowTitle("TCP Test");
    this->resize(700,450);

    linkBtn = new QPushButton(this);    
    linkBtn->move(30,30);
    linkBtn->resize(80,30);

    clearBtn = new QPushButton(this);    
    clearBtn->move(30,80);
    clearBtn->resize(80,30);

    serverIP = new QLineEdit(this);
    serverIP->move(130, 30);
    serverIP->resize(200, 30);

    serverPort = new QLineEdit(this);
    serverPort->move(340, 30);
    serverPort->resize(50, 30);

    linkBtn->setText(tr("连接"));
    clearBtn->setText(tr("清零"));
    serverIP->setText(tr("127.0.0.1"));
    serverPort->setText(tr("42345"));

    return;
}

void TCPHelper::destroyUI()
{
    delete linkBtn;
    delete clearBtn;
    delete serverIP;
    delete serverPort;
    return;
}
