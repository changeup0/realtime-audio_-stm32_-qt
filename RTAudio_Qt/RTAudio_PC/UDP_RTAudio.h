#ifndef UDP_RTAUDIO_H
#define UDP_RTAUDIO_H

#include <QTimer>
#include <QPushButton>
#include <QAudioFormat>
#include <QAudioOutput>

#include "UDPHelper.h"

class UDP_RTAudio : public UDPHelper
{
    Q_OBJECT
public:
    explicit UDP_RTAudio(void);
    ~UDP_RTAudio();

    void setAudioFmt();

private slots:
    void setUI(void);
    void destroyUI(void);

    void timer10ms(void);
    void playOrStop(void);

    virtual void readData(void); 

private:
    QTimer *pTimer;
    QPushButton *playBtn;    
    QAudioFormat *pAudioFmt;
    QAudioOutput *pAudioOut;

    quint16 db_dataCnt;
};

#endif
