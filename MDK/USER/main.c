#include "header.h"
#include "sys.h"

//系统工作时间
u32 clkCnt = 0;
// 定义音频帧的大小及缓冲
const u16 cu_audioFrameLen = 320;
u16 ia_audioBuf1[700], ia_audioBuf2[700];
bool b_buf1Flag = false, b_buf2Flag = false;
//音频帧采集计数
u32 getFrameCnt = 0;

void sendAudioFrame(u8 *ia_ptr, u16 len);

void Usart_test(void);

char ca_testStr[] = "ESP8266 Test";

char uint2StrBuf[50];
void uint2Str(u16 n);

int main(void)
{
	//临时变量
	//	u32 tempMs = 0, tmpSendCnt = clkCnt;
	u32 frameCnt = 0;
	//float tempFloat;
	// 设置中断优先级分组2
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
	//需要的资源初始化
	hardwareInit();

	while (1)
	{
		if (b_buf1Flag)
		{
			//			//sendAudioFrame((u8 *)ia_audioBuf1, cu_audioFrameLen*2);
			frameCnt++;
			//esp8266_sendAudioFrame((char *)ia_audioBuf1, cu_audioFrameLen * 2);

			b_buf1Flag = false;
			//			delay_ms(1);
			//			sprintf(uint2StrBuf, "%d %d", frameCnt, getFrameCnt);
			//			sprintf(uint2StrBuf, "%d\r\n", ADC_ConvertedValue);
			//			esp8266_sendAudioFrame(uint2StrBuf,7);
			//sendCnt(10);
		}
		if (b_buf2Flag)
		{
			//sendAudioFrame((u8 *)ia_audioBuf2, cu_audioFrameLen*2);
			frameCnt++;
			//esp8266_sendAudioFrame((char *)ia_audioBuf2, cu_audioFrameLen * 2);

			b_buf2Flag = false;
			//			delay_ms(1);
			//			sprintf(uint2StrBuf, "%d %d", frameCnt, getFrameCnt);
			//			esp8266_sendAudioFrame(uint2StrBuf, 7);
			//			//sendCnt(10);
		}
		delay_ms(500);
		LED3 = !LED3;
	};
	//	return 0;
}
// !!! Warn : no parameters check
void sendAudioFrame(u8 *ia_ptr, u16 len)
{
	while (len--)
	{
		USART1->DR = *ia_ptr++;
		while ((USART1->SR & 0x40) == 0)
			;
	}
	LED3 = !LED3;
}

void uint2Str(u16 n)
{
}

void Usart_test(void)
{
	if (USART1_RX_STA & 0x8000)
	{
		Usart2_Send(USART1_RX_BUF, 0, 1);
		printf("Send data to 8266:");
		printf(USART1_RX_BUF);
		printf("\r\n");
		LED3 = !LED3;
		USART1_RX_STA = 0;
	}
	if (USART2_RX_STA & 0x8000)
	{
		printf("Rec data from 8266:");
		printf(USART2_RX_BUF);
		printf("\r\n");
		USART2_RX_STA = 0;
	}
}
