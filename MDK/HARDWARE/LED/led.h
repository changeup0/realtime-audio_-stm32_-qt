#ifndef __LED_H
#define __LED_H	 
#include "sys.h"

typedef u8 bool;
#define true 1
#define false 0
	
#define led1Switch 0
#define led2Switch 0
#define led3Switch 1
#define led4Switch 0



#define LED0 PDout(2)



#if led1Switch
#define LED1_Port GPIOA
#define LED1_Pin GPIO_Pin_6
#define LED1 PAout(6)	 
#endif

#if led2Switch
#define LED2_Port GPIOA
#define LED2_Pin GPIO_Pin_7
#define LED2 PAout(7)	
#endif

#if led3Switch
#define LED3_Port GPIOB
#define LED3_Pin GPIO_Pin_0
#define LED3 PBout(0)
#endif

#if led4Switch
#define LED4_Port GPIOB
#define LED4_Pin GPIO_Pin_1
#define LED4 PBout(1)
#endif



void LED_Init(void);//��ʼ��

		 				    
#endif
