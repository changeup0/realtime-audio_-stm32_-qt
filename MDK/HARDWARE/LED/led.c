#include "led.h"
#include "header.h"

    
//LED IO初始化



void LED_Init(void)
{

	
 GPIO_InitTypeDef  GPIO_InitStructure;
 RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA|RCC_APB2Periph_GPIOB, ENABLE);	 //使能PC,PD端口时钟
 	
 GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 		
 GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;	


	
 #if led1Switch
 GPIO_InitStructure.GPIO_Pin = LED1_Pin;				
 GPIO_Init(LED1_Port, &GPIO_InitStructure);					
 GPIO_SetBits(LED1_Port,LED1_Pin);	
 #endif
 
 #if led2Switch
 GPIO_InitStructure.GPIO_Pin = LED2_Pin;				
 GPIO_Init(LED2_Port, &GPIO_InitStructure);					
 GPIO_SetBits(LED2_Port,LED2_Pin);	
 #endif
 
 #if led3Switch
 GPIO_InitStructure.GPIO_Pin = LED3_Pin;				
 GPIO_Init(LED3_Port, &GPIO_InitStructure);					
 GPIO_SetBits(LED3_Port,LED3_Pin);	
 #endif
 
 #if led4Switch
 GPIO_InitStructure.GPIO_Pin = LED4_Pin;				
 GPIO_Init(LED4_Port, &GPIO_InitStructure);					
 GPIO_SetBits(LED4_Port,LED4_Pin);	
 #endif
}
 
