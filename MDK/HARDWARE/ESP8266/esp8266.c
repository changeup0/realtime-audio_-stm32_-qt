#include "esp8266.h"

bool configFlag = 1;//通过串口配置esp8266的开关  忽略

/*************	本地常量声明	**************/
//const char *string = "AT+CIPSTART=\"TCP\",\"212.64.13.213\",9090";	//IP登录服务器
//const char *string_2="AT+CWJAP=\"MingZhi\",\"123123123\"";	     //WIFI 名字及密码
//const char *string_lamp1="newLamp;lamp1;a;b";    //WIFI 名字及密码
char cipmuxCmd[] = "AT+CIPMUX=0";
char transModeSetCmd[] = "AT+CIPMODE=1";
char transStartCmd[] = "AT+CIPSEND";
//esp8266模块信息
u8 esp8266Status = 0;
char esp8266SSID[] = "EspZjy";
//ip地址和端口信息
u8 ipStr[4] = {192,168,4,2};
u32 dtePort = 21234;
char connectTcpCmd[80];
//  寻找特定字符函数

u8 Find(char *a)
{ 
	USART2_RX_STA = 0;
  if(strstr(USART2_RX_BUF,a)!=NULL)
	    return 1;
	else
			return 0;
}

void CLR_Buf2()
{
	USART2_RX_BUF[0] = '\0';
}

void GetTcpAddr(void)
{
	sprintf(connectTcpCmd, "AT+CIPSTART=\"TCP\",\"%d.%d.%d.%d\",%d", ipStr[0], ipStr[1], ipStr[2], ipStr[3], dtePort);
}

void UART2_SendLR()
{
	Usart2_Send("\r\n", false, false);
}
//  指令发送函数
#define AtCmdFlag 1
bool Second_AT_Command(char *str, char * cmp, u16 wait_time, u8 tryTimes)         
{                                     
	u32 tmpMsBack = clkCnt;
	u8 tryCnt = 0;
	u8 reSendFlag = 1;
	CLR_Buf2(); 
	while(1)                    
	{
		if(clkCnt >= tmpMsBack+wait_time)
		{
			if(tryCnt > tryTimes)
			{
				return false;
			}
			reSendFlag = 1;
			tmpMsBack = 0xffffffff;
		}
		if(!Find(cmp)) 
		{	
			if(reSendFlag)
			{
				Usart2_Send((u8 *)str, false, false);
				Usart2_Send((u8 *)"\r\n", false, false);	
				tmpMsBack = clkCnt;
				reSendFlag = 0;
				tryCnt++;
				if(AtCmdFlag)Usart1_Send((u8 *)"Try...", false, true);
			}
    }
 	  else
		{
			if(AtCmdFlag)Usart1_Send((u8 *)"Cmd_End", false, true);
			return true;
		}
		if(wait_time == 0)
		{
			if(AtCmdFlag)Usart1_Send((u8 *)"Cmd_End", false, true);
			break;
		}
	}
	CLR_Buf2(); 
	return false;
}

//链接服务器函数

void Connect_Server(void)
{
//	  Second_AT_Command("AT","OK",1);//设置GPRS移动台类别为B,支持包交换和数据交换
//    Usart1_Send("设置WIFI模块为AT指令模式", false, true);	//关闭连接
//	  Second_AT_Command("AT+CWMODE=3","OK",2);//设置GPRS移动台类别为B,支持包交换和数据交换
//    Usart1_Send("设置WIFI模块为STA+AP模式", false, true);
//    Second_AT_Command("AT+RST","OK",2);//设置GPRS移动台类别为B,支持包交换和数据交换
//    Usart1_Send("复位模块", false, true);
//    Second_AT_Command("AT+CIFSR","OK",2);//设置GPRS移动台类别为B,支持包交换和数据交换
//    Usart1_Send("获取WIFI模块地址", false, true);
//    Second_AT_Command((char*)string_2,"OK",5);//设置GPRS移动台类别为B,支持包交换和数据交换
//    Usart1_Send("连接WIFI", false, true);
//    Second_AT_Command((char*)string,"OK",5);//设置GPRS移动台类别为B,支持包交换和数据交换
//    Usart1_Send("建立TCP连接", false, true);
//    Second_AT_Command("AT+CIPMODE=1","OK",2);//设置GPRS移动台类别为B,支持包交换和数据交换
//    Usart1_Send("设置透传模式成功", false, true);
//	  delay_ms(100);     
//   	CLR_Buf2();
}

//接收服务器字符函数
//void Rec_Server_Data(void)
//{
//	if(plast!=pFir)   		
//	{	
//		*pNew=*plast;
//		plast++;
//    if(plast>&Uart2_Buf[Buf_Max])
//			plast=Uart2_Buf;
//	}
//}

//向服务器发送字符函数
//void Send_information(void)
//{
//	Second_AT_Command("AT+CIPSEND",">",2);
//	Usart2_Send((u8 *)string_lamp1, false, true); 
//}




void ESP8266_Init()
{
	socketinfo socket;
	
//		configFlag = true; //配置模式开启
//		Exit_Pass_Through();
//		Second_AT_Command("AT+CWMODE=3","OK",100,5);
//		Second_AT_Command("AT+RST","OK",0, 5);
//	  if(AtCmdFlag)Usart1_Send("测试WIFI模块开始", false, true); 	//关闭连接
//		if(Second_AT_Command("AT","OK", 300, 5))
//		{
//			esp8266Status = 1;
//	delay_ms(200);
//	Second_AT_Command("AT+CWMODE=3","OK",100,5);
//	Second_AT_Command("AT+RST","OK",0, 5);
//			delay_s(2);
//		}
//		configFlag = false; //配置模式关闭
//	  if(AtCmdFlag)Usart1_Send("连接成功\r\n", false, true); 
	esp8266_cmd("ATE0");//关闭8266回显
	strcpy(socket.ip, destIP);
	socket.port = destPort;
	udp_connect(&socket);
	printf("\r\nUdp OK\r\n");
}

void ESP8266_Test(void)
{
	Exit_Pass_Through();
	if(Second_AT_Command("AT","OK", 400, 5))
		esp8266Status = true;
	else
		esp8266Status = false;
}

void Exit_Pass_Through(void)
{
	  Usart2_Send("+++",false, false);//退出透传模式，避免模块还处于透传模式中
		delay_ms(200);
		Usart2_Send("+++",false, false);//退出透传模式，避免模块还处于透传模式中
		delay_ms(200);
		Usart2_Send("+++",false, false);//退出透传模式，避免模块还处于透传模式中
		delay_ms(200);
		esp8266Status = 1;
}



/***************My methods*********************/


char msgBuf[200];

void esp8266_cmd(u8 * cmdStr){
	esp8266_sendData((char *)cmdStr, "OK");
}

void udp_connect(socketptr socketPtr){
	sprintf(msgBuf,"AT+CIPSTART=\"UDP\",\"%s\",%d,7000", socketPtr->ip, socketPtr->port);
	esp8266_sendData(msgBuf, "CONNECT");
}

void esp8266_sendData(char * msg, char * pattern){
	int timeCnt = 10;
	while(timeCnt--){
		if(USART2_RX_STA & 0x8000){
			printf("Recieve0: ");
			printf(USART2_RX_BUF);
			if(strstr((const char *)USART2_RX_BUF,pattern) != NULL){
				break;
			}
			else{
				Usart2_Send((u8 *)msg, 0,1);
				printf("Send0: \r\n");
				printf(msg);printf("\r\n");
			}
			USART2_RX_STA = 0;
			//printf(USART_RX_BUF);
		}
		else{
			Usart2_Send((u8 *)msg, 0,1);
			 printf("Send1: \r\n"); 
			 printf(msg);
		}
		delay_ms(1000);
	}
}

void esp8266_sendMsg(char * msg, char * pattern){
	u16 len = strlen((const char*)msg);
//	u8 cnt = 10;
	sprintf(msgBuf,"AT+CIPSEND=%d", len);
//	while(cnt--){
		Usart2_Send((u8 *)msgBuf, 0,1);// huanhangfu 0 or 1
		USART2_RX_STA = 0;
		Usart2_Send((u8 *)msg, 0,1);
		USART2_RX_STA = 0;
//		printf("Recieve:\r\n");
//		printf(USART2_RX_BUF);
//		printf("\r\n");
//		if(strstr((const char *)USART2_RX_BUF,pattern) != NULL){
//			break;
//		}
//		USART2_RX_STA = 0;
//	}
}

void esp8266_sendAudioFrame(char *buf, u16 len)
{
	u16 t;
	sprintf(msgBuf,"AT+CIPSEND=%d", len);
	Usart2_Send((u8 *)msgBuf,0,1);
	delay_ms(1);
	for(t=0;t<len;t++)
	{
		USART2->DR=buf[t];
		while((USART2->SR&0X40)==0);//等待发送结束
	}
}

