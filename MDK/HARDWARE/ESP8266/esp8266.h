#ifndef __ESP8266_H
#define __ESP8266_H


#include "usart.h"

#include "string.h"



#define destIP "255.255.255.255"
#define destPort 7000


extern u8 esp8266Status;
extern char esp8266SSID[];
extern char transModeSetCmd[];
extern char transStartCmd[];
extern char connectTcpCmd[];
extern char cipmuxCmd[];

extern u8 ipStr[];
extern u32 dtePort;
extern u8 ipAddr[];


/*************	本地函数声明	**************/

u8 Find(char *a);
u8 Second_AT_Command(char *str, char * cmp, u16 wait_time, u8 tryTimes);
void Connect_Server(void);
void Wait_CREG(void);
void Send_information(void);
void ESP8266_Init(void);
void ESP8266_Test(void);
void GetTcpAddr(void);
void Exit_Pass_Through(void);

// My esp8266 methods
typedef struct SOCKETINFO{
	char ip[20];
	u16 port;
}socketinfo, *socketptr;

extern char msgBuf[200];

void esp8266_cmd(u8 * cmdStr);
void udp_connect(socketptr socketPtr);
void esp8266_sendData(char * msg, char * pattern);
void esp8266_sendMsg(char * msg, char * pattern);

//注意修改数据帧长度(为了加快发送速度)
void esp8266_sendAudioFrame(char *buf, u16 len);

#endif

