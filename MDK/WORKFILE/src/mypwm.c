#include "mypwm.h"


mypwm pwm1 = {0, 25, 10};
mypwm pwm2 = {0, 25, 15};
mypwm pwm3 = {0, 25, 20};
mypwm pwm4 = {0, 25, 25};

void MyPwmInit()
{
 #if !pwmLed
 GPIO_InitTypeDef  GPIO_InitStructure;
 RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);	 //ʹ��PA,PD�˿�ʱ��
	
 GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 		
 GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;		
 
 #if pwm1Switch
 GPIO_InitStructure.GPIO_Pin = PWM1_Pin;				
 GPIO_Init(PWM1_Port, &GPIO_InitStructure);					
 GPIO_SetBits(PWM1_Port,PWM1_Pin);						
 #endif
	
 #if pwm2Switch
 GPIO_InitStructure.GPIO_Pin = PWM2_Pin;				
 GPIO_Init(PWM2_Port, &GPIO_InitStructure);					
 GPIO_SetBits(PWM2_Port,PWM2_Pin);						
 #endif
	
#if pwm3Switch
 GPIO_InitStructure.GPIO_Pin = PWM3_Pin;				
 GPIO_Init(PWM3_Port, &GPIO_InitStructure);				
 GPIO_SetBits(PWM3_Port,PWM3_Pin);						
 #endif
 
 #if pwm4Switch
 GPIO_InitStructure.GPIO_Pin = PWM4_Pin;				 
 GPIO_Init(PWM4_Port, &GPIO_InitStructure);					
 GPIO_SetBits(PWM4_Port,PWM4_Pin);						 
 #endif
 #endif
}
void MyPwmSetDuty(u8 n, u8 duty)
{
	duty += duty*3;
	switch(n)
	{
		case 1:
			pwm1.duty = duty;
		break;
		case 2:
			pwm2.duty = duty;
		break;
		case 3:
			pwm3.duty = duty;
		break;
		case 4:
			pwm4.duty = duty;
		break;
		default:break;
	}
}

