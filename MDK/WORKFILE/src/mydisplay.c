#include "mydisplay.h"
#include "myoled.h"
#include "oled.h"

void DisplayVolt(u8 x, u8 y, float v)
{
	u8 vInt = v;
	u8 vDec = (v - vInt) * 100;
	OLED_Write_String(x, y, "Volt:");
	OLED_Write_Num1(x+6, y, vInt);
	OLED_Write_String(x+7, y, ".");
	OLED_Write_Num2(x+8, y, vDec);
	OLED_Write_String(x+11, y, "V");
}

void GetVoltAndDisplay(u8 x, u8 y)
{
	DisplayVolt(0, 0, (float)Get_Adc(1)*3.3/4096);
}





