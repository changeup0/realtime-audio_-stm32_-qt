#include "mykey.h"

mykey key1 = {0, 0, 100};
mykey key2 = {0, 0, 100};
mykey key3 = {0, 0, 100};
mykey key4 = {0, 0, 100};

void MyKeyInit(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA,ENABLE);//使能PORTA,PORTC时钟

	//GPIO_PinRemapConfig(GPIO_Remap_SWJ_JTAGDisable, ENABLE);//关闭jtag，使能SWD，可以用SWD模式调试
	
	#if key1Switch
	GPIO_InitStructure.GPIO_Pin = KEY1_Pin;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU; //设置成上拉输入
 	GPIO_Init(KEY1_Port, &GPIO_InitStructure);
	#endif
	#if key2Switch
	GPIO_InitStructure.GPIO_Pin  = KEY2_Pin;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU; //设置成上拉输入
 	GPIO_Init(KEY2_Port, &GPIO_InitStructure);
  #endif
	#if key3Switch
	GPIO_InitStructure.GPIO_Pin  = KEY3_Pin;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU; //PA0设置成输入，默认下拉	  
	GPIO_Init(KEY3_Port, &GPIO_InitStructure);
	#endif
	#if key4Switch
	GPIO_InitStructure.GPIO_Pin  = KEY4_Pin;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU; //设置成上拉输入
 	GPIO_Init(KEY4_Port, &GPIO_InitStructure);
	#endif
}


