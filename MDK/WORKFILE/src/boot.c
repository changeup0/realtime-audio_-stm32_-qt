#include "boot.h"
#include "header.h"

//初始化所需要的资源
void hardwareInit(void)
{
	delay_init();	     						 //延时初始化
	LED_Init();				 						 //初始化LED          // A6、A7、B0 、B1
//	My_OLED_Init();	
	Usart1_init(115200);   //串口1   A9  A10   调试使用
  Usart2_init(921600);   //串口2   A2  A3    ESP8266
//	IIC_Init();	
//	MPU6050_initialize();	
//	DMP_Init();
		if(WM8978_Init())			//内部初始化 IIS 和 IIC
		{
			printf("wm8978 init error\r\n");
		}
//	ESP8266_Init();
//	Adc_Init();
//	TIM3_Int_Init(35,124);				 //定时中断初始化     1ms中断  				 TIM_Cmd ; 
	

//	MyPwmInit();
//	MyKeyInit();
#ifdef DEBUG
	printf("Init Finished  !\n");
#endif
}


