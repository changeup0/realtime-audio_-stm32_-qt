#include "cmddeal.h"
#include "header.h"

#define HC05_Send Usart2_Send

bool CmdMode_EN = true;
bool Usart1_CmdMode_EN = true;
bool Usart2_CmdMode_EN = true;
#define cmdCnt 9  //指令集的指令个数
#define default_usart	Usart1_Send    //默认指令串口
#define PARAM_MAX 3
#define PARAM_LEN 6
#define CMDLEN_MAX 10
u32 tempParamArray[PARAM_MAX];
u8 paramStr[PARAM_MAX][PARAM_LEN];
char tempCmdArray[CMDLEN_MAX+1];
u8 ledWaterType = 0;//0-9   + 3 2.6 1.8 1 0.5 0.25 0.1 0.05 0.02
u8 ledWaterSta = 0;
u32 ledWaterTime = 400;
u8 ledBrignt = 5;
cmd cmdInfo[cmdCnt] =
{
	{"?",			  "[ help information ]" , 0, 0, Cmd_Help},
	{"+", 			"\"+\" ledNumber	[	ledNumber on  ]" , 1, 1, LED_Light},
	{"-",				"\"-\" ledNumber	[	ledNumber off ]" , 1, 2, LED_PutOut},
	{"*", 			"\"*\" statNumber	 	[	led as statNumber	]" , 1, 3, LED_Control},
	{"LM", 			"\"LM\" 0-3		[	change a water-led type	]" , 1, 4, LED_WaterType},
	{"LS", 			"\"LS\" 0-9		[	led as statNumber	]" , 1, 5, LED_WaterSpeed},
	{"HM", 			"\"HM\" 0-9		[	led as statNumber	]" , 1, 6, LED_WaterBrightness},
	
	
	
	
	{"TEST", 		"\"TEST\" [	WORK STATE	]" , 0, 7, Cmd_WorkState},
	{"delayms", "\"delayms\" N  [	delay N ms ]" , 1, 8, Cmd_delay_test}
};

cmdset myCmdSet = {cmdInfo, default_usart, Cmd_Exec};

void Cmd_detect_Usart1(u8 sta)
{
	switch(sta)
	{
		case CMD_ERROR:
			Usart1_Send("CMD_ERROR", false, true);
		break;
		case CMD_NOT:
			Usart1_Send("CMD_NOT", false, true);
		break;
		case PARAM_ERROR:
			Usart1_Send("PARAM_ERROR", false, true);
			break;
		case EXEC_OK:
			Usart1_Send("EXEC_OK", false, true);
		break;
		default:
			Usart1_Send("Cmd No ERROR", false, true);
		break;
	}
}

void Cmd_detect_Usart2(u8 sta)
{
	switch(sta)
	{
		case CMD_ERROR:
			HC05_Send("CMD_ERROR", false, true);
		break;
		case CMD_NOT:
			HC05_Send("CMD_NOT", false, true);
		break;
		case PARAM_ERROR:
			HC05_Send("PARAM_ERROR", false, true);
			break;
		case EXEC_OK:
			HC05_Send("EXEC_OK", false, true);
		break;
		default:
			HC05_Send("Cmd No ERROR", false, true);
		break;
	}
}
u8 Cmd_Scan(u8 * CmdStr)
{
	u8 timeStr[8];
	u16 tmpParaCnt = 0, i;
	u32 timeCnt;
	tmpParaCnt = CountArray(' ', (char *)CmdStr);
	if(tmpParaCnt > PARAM_MAX)	return CMD_ERROR;
	i = FindArray(0, ' ', (char *)CmdStr);
	if(i == NOT_FIND)
		i = strlen((const char *) CmdStr);
	if(i >= CMDLEN_MAX)	return CMD_ERROR;
	memcpy(tempCmdArray, CmdStr, i);
	tempCmdArray[i] = '\0';
	for(i = 0; i < cmdCnt; ++ i)
	{
		if(strcmp((const char *)(myCmdSet.cmdList[i].keyStr), tempCmdArray) == 0)
		{
			if(tmpParaCnt != myCmdSet.cmdList[i].paramCnt || CmdStr[strlen((const char *) CmdStr)-1] == ' ')
			{
				return PARAM_ERROR;
			}
			else
			{
				if(tmpParaCnt)
					Param_Get(CmdStr);
				timeCnt = Cmd_Exec(myCmdSet.cmdList[i]);
				sprintf((char *)timeStr, "%d ms", timeCnt);
				myCmdSet.infoSend(timeStr, false, true);
				return 	EXEC_OK;
			}
		}
	}
	return CMD_NOT;
}

void Param_Get(u8 *tmpCmdStr)
{
	u16 i, tmpIndex = 0, tmpParamCnt = 0;
	tmpIndex = FindArray(tmpIndex, ' ', (char *)tmpCmdStr)+1;
	while((i = FindArray(tmpIndex, ' ', (char *)tmpCmdStr)) != NOT_FIND)
	{
		memcpy(paramStr[tmpParamCnt], tmpCmdStr+tmpIndex, i-tmpIndex);
		paramStr[tmpParamCnt][i-tmpIndex] = '\0';
		tmpIndex = i;
		tmpParamCnt++;
	}
	i = strlen((const char *)tmpCmdStr)-tmpIndex;
	memcpy(paramStr[tmpParamCnt], tmpCmdStr+tmpIndex, i);
	paramStr[tmpParamCnt][i] = '\0';
	for(i = 0; i <= tmpParamCnt; ++ i)
	{
		if(strlen((const char *)paramStr[i]) == 1)
			tempParamArray[i] = *paramStr[i];
		else
			tempParamArray[i] = (u32)paramStr[i];
	}
}


u32 Cmd_Exec(cmd command)
{
	u32 tBack = clkCnt;
	switch(command.paramCnt)
	{
		case 0://无参数(void类型)											  
			(*(u32(*)())command.func)();
			break;
	    case 1://有1个参数
			(*(u32(*)())command.func)(tempParamArray[0]);
			break;
	    case 2://有2个参数
			(*(u32(*)())command.func)(tempParamArray[0],tempParamArray[1]);
			break;
	    case 3://有3个参数
			(*(u32(*)())command.func)(tempParamArray[0],tempParamArray[1],tempParamArray[2]);
			break;
	}
	return clkCnt - tBack;
}

void Trans_srcData()
{
	
}

void Trans_resData()
{
	
}
/***********以下为指令函数***************/
void Cmd_Help(void)
{
	u8 i;
	myCmdSet.infoSend("This is a usart test process\n", false, true);
	myCmdSet.infoSend("You can send a cmd as this format:	关键字(指令id) 参数1 参数2 参数3\n", false, true);
	myCmdSet.infoSend("关键字\"help\" 会给出可用指令信息清单\n", false, true);
	for(i = 0 ; i < cmdCnt; ++ i)
	{
		myCmdSet.infoSend((u8 *)myCmdSet.cmdList[i].cmdformat, false, true);
	}
}


void LED_Light(u8 n)
{
	if(n >= 0x30)
		n -= 0x30;
	switch(n)
	{
		case 1:LED1_PWM_EN=true;ledStatus.led1 = true;break;
		case 2:LED2_PWM_EN=true;ledStatus.led2 = true;break;
		case 3:LED3_PWM_EN=true;ledStatus.led3 = true;break;
		case 4:LED4_PWM_EN=true;ledStatus.led4 = true;break;
	}
}
void LED_PutOut(u8 n)
{
	if(n >= 0x30)
		n -= 0x30;
//	switch(n)
//	{
//		case 1:LED1_PWM_EN=false;LED1=1;ledStatus.led1 = false;break;
//		case 2:LED2_PWM_EN=false;LED2=1;ledStatus.led2 = false;break;
//		case 3:LED3_PWM_EN=false;LED3=1;ledStatus.led3 = false;break;
//		case 4:LED4_PWM_EN=false;LED4=1;ledStatus.led4 = false;break;
//	}
}
void LED_Control(u8 * sParam)
{
	u8 i = 0;
	while(i++ < 4)
	{
		if(*(sParam++)-0x30)
			LED_Light(i);
		else
			LED_PutOut(i);
	}
}



void LED_WaterType(u8 n)
{
	if(n >= 0x30)
		n -= 0x30;
	ledWaterType = n;
	ledWaterSta = 0;
	if(n == 0)
	{
		LED_WaterControl("0000");
		LED_Light(1);LED_Light(2);LED_Light(3);LED_Light(4);
	}
}
void LED_WaterSpeed(u8 n)//0-9   + 3 2.6 1.8 1 0.5 0.25 0.1 0.05 0.02
{
	if(n >= 0x30)
		n -= 0x30;
	switch(n)
	{
		case 0:ledWaterTime = 0;break;
		case 1:ledWaterTime = 3000;break;
		case 2:ledWaterTime = 2600;break;
		case 3:ledWaterTime = 1800;break;
		case 4:ledWaterTime = 1000;break;
		case 5:ledWaterTime = 500;break;
		case 6:ledWaterTime = 250;break;
		case 7:ledWaterTime = 100;break;
		case 8:ledWaterTime = 50;break;
		case 9:ledWaterTime = 20;break;
	}
}

void LED_WaterBrightness(u8 n)
{
	if(n >= 0x30)
		n -= 0x30;
	
	ledBrignt = n;
	if(ledWaterType <= 0)
	{
		MyPwmSetDuty(1, n);
		MyPwmSetDuty(2, n);
		MyPwmSetDuty(3, n);
		MyPwmSetDuty(4, n);
	}
}

void DATA_Trans(u8 *tParam, u8 type)
{
	
}
void LED_WaterControl(u8 * sParam)
{
	u8 i = 0;
	while(i++ < 4)
	{
		MyPwmSetDuty(i, (*(sParam++)-0x30)*ledBrignt);
	}
}




















void Cmd_delay_test(u8 * pT)
{
	u32 tmpTime;
	sscanf((char *)pT, "%u", &tmpTime);
	if(tmpTime > 1000)
	{
		delay_s(tmpTime/1000);
		tmpTime %= 1000;
	}
	delay_ms(tmpTime);
}


void Cmd_NumSend(u32 n)
{
	char tmpStr[10];
	sprintf(tmpStr, "%d", n);
	myCmdSet.infoSend((u8*)tmpStr, false, true);
}
void Cmd_WorkState()
{
	myCmdSet.infoSend("LED_Water_Mode:", false, false);
	Cmd_NumSend(ledWaterType);
	myCmdSet.infoSend("LED_WaterSpeed:", false, false);
	Cmd_NumSend(ledWaterTime);
	myCmdSet.infoSend("LED_WaterLight:", false, false);
	Cmd_NumSend(ledBrignt);
}


