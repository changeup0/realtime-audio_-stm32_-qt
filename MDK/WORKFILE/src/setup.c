#include "setup.h"
#include "header.h"
#include "adc.h"
//为了使用方便 定义的一些开关

bool LED1_PWM_EN = false;
bool LED2_PWM_EN = false;
bool LED3_PWM_EN = false;
bool LED4_PWM_EN = false;

bool USART1_EN = true;
bool USART2_EN = true;
bool USART3_EN = false;

bool MPU6050_EN = true;

bool KEY_EN = true;

u16 *ptrGetVal = NULL;

void TIM3_IRQHandler(void) //TIM3中断
{
	if (TIM_GetITStatus(TIM3, TIM_IT_Update) != RESET) //检查指定的TIM中断发生与否:TIM 中断源
	{
		//////////////////////////////////////////////////////////////////////////////////////////系统工作时间统计
		if (clkCnt >= cu_audioFrameLen)
		{
			if (getFrameCnt & 0x00000001)
			{
				b_buf1Flag = true;
				b_buf2Flag = false;
				ptrGetVal = (u16 *)ia_audioBuf2;
			}
			else
			{
				b_buf2Flag = true;
				b_buf1Flag = false;
				ptrGetVal = (u16 *)ia_audioBuf1;
			}
			clkCnt = 0;
			getFrameCnt++;
			//esp8266_sendAudioFrame("fill a buffer\r\n");
		}
		*ptrGetVal++ = ADC_ConvertedValue;
		clkCnt++;
#if 0
//		//////////////////////////串口处理
//		if(USART1_RX_STA > 0 && (USART1_RX_STA&0x8000) == 0 && clkCnt - Usart1_Rec_Ing > 50)
//		{
//			USART1_RX_BUF[USART1_RX_STA&0X3FFF] = '\0';
//			Usart1_Rx_Len = USART1_RX_STA;
//			Usart1_Rec_Cnt += USART1_RX_STA;
//			USART1_RX_STA|=0x8000;
//			usartRefreshFlag = 1;
//			frame1HeadCmpCnt=0;startTrans1Flag=false;
//		}
		////////////////////////////////////////////////////////////////////////////////////////流水灯处理阶段
		if(LED_WaterType > 0 && ledWaterTime > 0 && clkCnt % ledWaterTime == 0)
		{
			if(ledWaterType == 1)
			{
				switch(ledWaterSta % 4)
				{
					case 0:LED_WaterControl("0001");break;
					case 1:LED_WaterControl("0010");break;
					case 2:LED_WaterControl("0100");break;
					case 3:LED_WaterControl("1000");break;
				}
			}
			else if(ledWaterType == 2)
			{	
				switch(ledWaterSta % 6)
				{
					case 0:LED_WaterControl("0001");break;
					case 1:LED_WaterControl("0010");break;
					case 2:LED_WaterControl("0100");break;
					case 3:LED_WaterControl("1000");break;
					case 4:LED_WaterControl("0100");break;
					case 5:LED_WaterControl("0010");break;
				}
			}
			else if(ledWaterType == 3)
			{
				switch(ledWaterSta % 4)
				{
					case 0:LED_WaterControl("0011");break;
					case 1:LED_WaterControl("0110");break;
					case 2:LED_WaterControl("1100");break;
					case 3:LED_WaterControl("0110");break;
				}
			}
			ledWaterSta++;
		}
		
		//////////////////////////////////////////////////////////////////////////////////////////PWM处理阶段
#if (pwm1Switch && ((!pwmLed) || (led1Switch)))
		pwm1.freCnt++;
		if(pwm1.freCnt >= pwm1.freCntMax) pwm1.freCnt = 0;
		if(LED1_PWM_EN && pwmLed){
		if(pwm1.freCnt >= pwm1.duty)	
			PWM1_Status = 1;
		else
			PWM1_Status = 0;
	 }
#endif
#if (pwm2Switch && ((!pwmLed) || (led2Switch)))
		pwm2.freCnt++;
		if(pwm2.freCnt >= pwm2.freCntMax) pwm2.freCnt = 0;
		if(LED2_PWM_EN && pwmLed){
		if(pwm2.freCnt >= pwm2.duty)	
			PWM2_Status = 1;
		else
			PWM2_Status = 0;
	 }
#endif
#if (pwm3Switch && ((!pwmLed) || (led3Switch)))
		pwm3.freCnt++;
		if(pwm3.freCnt >= pwm3.freCntMax) pwm3.freCnt = 0;
		if(LED3_PWM_EN && pwmLed){
		if(pwm3.freCnt >= pwm3.duty)	
			PWM3_Status = 1;
		else
			PWM3_Status = 0;
  	}
#endif
#if (pwm4Switch && ((!pwmLed) || (led4Switch)))
		pwm4.freCnt++;
		if(pwm4.freCnt >= pwm4.freCntMax) pwm4.freCnt = 0;
		if(LED4_PWM_EN && pwmLed){
		if(pwm4.freCnt >= pwm4.duty)	
			PWM4_Status = 1;
		else
			PWM4_Status = 0;
		}
#endif
	
		/////////////////////////////////////////////////////////////////////////////////////////按键处理阶段
#if key1Switch
		if(KEY1_Status == 0)
			(key1.avaiCnt < 50000) ? (key1.avaiCnt++) : (key1.avaiCnt=50000);
		else
		{
			if(key1.avaiCnt > 10*key1.avaiCntMax) key1.pressCnt = 2;
			else if(key1.avaiCnt > key1.avaiCntMax) key1.pressCnt = 1;
			key1.avaiCnt = 0;
		}
#endif
#if key2Switch
		if(KEY2_Status == 0)
			(key2.avaiCnt < 50000) ? (key2.avaiCnt++) : (key2.avaiCnt=50000);
		else
		{
			if(key2.avaiCnt > 10*key2.avaiCntMax) key2.pressCnt = 2;
			else if(key2.avaiCnt > key2.avaiCntMax) key2.pressCnt = 1;
			key2.avaiCnt = 0;
		}
#endif
#if key3Switch
		if(KEY3_Status == 0)
			(key3.avaiCnt < 50000) ? (key3.avaiCnt++) : (key3.avaiCnt=50000);
		else
		{
			if(key3.avaiCnt > 10*key3.avaiCntMax) key3.pressCnt = 2;
			else if(key3.avaiCnt > key3.avaiCntMax) key3.pressCnt = 1;
			key3.avaiCnt = 0;
		}
#endif
#if key4Switch
		if(KEY4_Status == 0)
			(key4.avaiCnt < 50000) ? (key4.avaiCnt++) : (key4.avaiCnt=50000);
		else
		{
			if(key4.avaiCnt > 10*key4.avaiCntMax) key4.pressCnt = 2;
			else if(key4.avaiCnt > key4.avaiCntMax) key4.pressCnt = 1;
			key4.avaiCnt = 0;
		}
#endif
		
		/////////////////////////////////////////////////////////////////////////////////////////菜单同步阶段
  	(LED1_PWM_EN && pwm1.duty > 3) ? (myMenuItem1[0].flag = true) : (myMenuItem1[0].flag = false);
		(LED2_PWM_EN && pwm2.duty > 3) ? (myMenuItem1[1].flag = true) : (myMenuItem1[1].flag = false);
		(LED3_PWM_EN && pwm3.duty > 3) ? (myMenuItem1[2].flag = true) : (myMenuItem1[2].flag = false);
		(LED4_PWM_EN && pwm4.duty > 3) ? (myMenuItem1[3].flag = true) : (myMenuItem1[3].flag = false);
		(key1.avaiCnt) ? (myMenuItem1[4].flag = true) : (myMenuItem1[4].flag = false);
		(key2.avaiCnt) ? (myMenuItem1[5].flag = true) : (myMenuItem1[5].flag = false);
		(key3.avaiCnt) ? (myMenuItem1[6].flag = true) : (myMenuItem1[6].flag = false);
		(key4.avaiCnt) ? (myMenuItem1[7].flag = true) : (myMenuItem1[7].flag = false);
    myMenuItem1[8].value = adcSrcValue;
		myMenuItem1[9].value = accel[0];
		myMenuItem1[10].value = accel[1];
		myMenuItem1[11].value = accel[2];
		myMenuItem1[12].value = gyro[0];
		myMenuItem1[13].value = gyro[1];
		myMenuItem1[14].value = gyro[2];
		
		myMenuItem2[0].value = accel[0];
		myMenuItem2[1].value = accel[1];
		myMenuItem2[2].value = accel[2];
		myMenuItem2[3].value = gyro[0];
		myMenuItem2[4].value = gyro[1];
		myMenuItem2[5].value = gyro[2];
		myMenuItem2[6].value = Pitch;
		myMenuItem2[7].value = Roll;
		myMenuItem2[8].value = Yaw;
		
		myMenuItem3[0].value = Usart1_Rec_Cnt;
		//myMenuItem3[1].value = Usart1_Send_Cnt;
		
		//esp8266-usart2
		if(esp8266Status < 4)
			configFlag = 1;
		else
			configFlag = 0;
#endif

		TIM_ClearITPendingBit(TIM3, TIM_IT_Update); //清除TIMx的中断待处理位:TIM 中断源
	}
}
