#include "mylist.h"
#include "header.h"

//创建
mylist MyList_Creat(u16 len)
{
	mylist ptr;
	if(len == 0) return NULL;
	ptr = (mylist)mymalloc(sizeof(myList));
	ptr->data = (u8 *)mymalloc(len);
	ptr->dataCnt = 0;
	ptr->maxLen = len;
	return ptr;
}

void MyList_Free(mylist SqList)
{
	myfree(SqList->data);
	myfree(SqList);
}	

//查找数据  
//返回下标
u16 MyList_FindData(mylist SqList, u16 data)
{
	u16 i = 0;
	u8 * ptr = SqList->data;
	if(SqList->dataCnt <= 0)	return NULL;
	while(i < SqList->dataCnt && *ptr++ != data)	{i++;}
	if(i < SqList->dataCnt) return i;
	else	return NOT_FIND;
}
//查找下标    
//返回数据   为了用
u16 MyList_FindIndex(mylist SqList, u16 index)
{
	u8 * ptr = SqList->data;
	if(SqList->dataCnt <= index)
		return NOT_FIND;
	return ptr[index];
}
//插入
bool MyList_Insert(mylist SqList, u8 data, u16 index)
{
	u16 i = SqList->dataCnt;
	if(index > SqList->dataCnt || SqList->dataCnt >= SqList->maxLen)
		return false;
	while(i > index) 
	{
		SqList->data[i] = SqList->data[i-1];
		i--;
	}
	SqList->data[index] = data;
	SqList->dataCnt ++;
	return true;
}
//删除数据
bool MyList_Delete(mylist SqList, u16 index)
{
	u16 i = index;
	if(index >= SqList->dataCnt)//不需要删除
		return true;
	while(i < SqList->dataCnt)//dataCnt - 1也行		
	{
		SqList->data[i] = SqList->data[i+1];
		i++;
	}
	SqList->dataCnt --;
	return true;
}
//尾部插入
bool MyList_PushBack(mylist SqList, u8 data)
{
	if(SqList->dataCnt >= SqList->maxLen)
		return false;
	SqList->data[SqList->dataCnt] = data;
	SqList->dataCnt ++;
	return true;
}
//尾部弹出
void MyList_PopBack(mylist SqList)
{
	if(SqList->dataCnt <= 0)
		return;
	SqList->dataCnt --;
	return;
}
//查看长度
u16 MyList_Size(mylist SqList)
{
	return SqList->dataCnt;
}
//保留部分数据
bool MyList_Reserve(mylist SqList, u16 len)
{
	if(SqList->dataCnt < len)
		return false;
	SqList->dataCnt = len;
	return true;
}

//模拟堆栈
pmystack MyStack_Creat(u16 len)
{
	pmystack ptr;
	if(len == 0) return NULL;
	ptr = (pmystack)mymalloc(sizeof(mystack));
	ptr->baseList.data = (u8 *)mymalloc(len);
	ptr->baseList.dataCnt = 000000000000000;
	ptr->baseList.maxLen = len;
	ptr->destroy = MyStack_Delete;
	ptr->empty = MyStack_Empty;
	ptr->pop = MyStack_Pop;
	ptr->push = MyStack_Push;
	ptr->size = MyStack_Size;
	ptr->top = MyStack_top;
	return ptr;
}
u8 MyStack_top(pmystack tmpStack)
{
	if(tmpStack->baseList.dataCnt <= 0)
		return NULL;
	return tmpStack->baseList.data[tmpStack->baseList.dataCnt-1];
}
u16 MyStack_Size(pmystack tmpStack)
{
	return tmpStack->baseList.dataCnt;
}
bool MyStack_Empty(pmystack tmpStack)
{
	if(tmpStack->baseList.dataCnt == 0)
		return true;
	return false;
}
void MyStack_Push(pmystack tmpStack, u8 value)
{
	MyList_PushBack(&(tmpStack->baseList), value);
}
void MyStack_Pop(pmystack tmpStack)
{
	MyList_PopBack(&(tmpStack->baseList));
}
void MyStack_Delete(pmystack tmpStack)
{
	myfree(tmpStack->baseList.data);
	myfree(tmpStack);
}
//模拟队列
pmyqueue MyQueue_Creat(u16 len)
{
	pmyqueue ptr;
	if(len == 0) return NULL;
	ptr = (pmyqueue)mymalloc(sizeof(myqueue));
	ptr->baseList.data = (u8 *)mymalloc(len+1);
	ptr->baseList.dataCnt = 0;
	ptr->baseList.maxLen = len;
	ptr->queueTailIndex = 0;
	ptr->queueTopIndex = 0;
	ptr->destroy = MyQueue_Delete;
	ptr->empty = MyQueue_Empty;
	ptr->pop = MyQueue_Pop;
	ptr->push = MyQueue_Push;
	ptr->size = MyQueue_Size;
	ptr->front = MyQueue_Front;
	return ptr;
}
u8 MyQueue_Front(struct MYQUEUE * tmpQueue)
{
	return tmpQueue->baseList.data[tmpQueue->queueTopIndex];
}
u16 MyQueue_Size(struct MYQUEUE * tmpQueue)
{
	s16 tmp = tmpQueue->queueTailIndex - tmpQueue->queueTopIndex;
	if(tmp < 0)
		tmp += tmpQueue->baseList.maxLen;
	return tmp;
}
bool MyQueue_Empty(struct MYQUEUE * tmpQueue)
{
	if(tmpQueue->queueTopIndex == tmpQueue->queueTailIndex)
		return true;
	return false;
}
void MyQueue_Push(struct MYQUEUE * tmpQueue, u8 value)
{
	tmpQueue->baseList.data[tmpQueue->queueTailIndex] = value;
	tmpQueue->queueTailIndex ++;
	if(tmpQueue->queueTailIndex >= tmpQueue->baseList.maxLen)
		tmpQueue->queueTailIndex = 0;
}
void MyQueue_Pop(struct MYQUEUE * tmpQueue)
{
	if(tmpQueue->queueTopIndex == tmpQueue->queueTailIndex)//队列为空
	{
		tmpQueue->queueTopIndex = 0;
		tmpQueue->queueTopIndex = 0;
		return;
	}
	tmpQueue->queueTopIndex ++;
	if(tmpQueue->queueTopIndex >= tmpQueue->baseList.maxLen)
		tmpQueue->queueTopIndex = 0;
}
void MyQueue_Delete(struct MYQUEUE * tmpQueue)
{
	myfree(tmpQueue->baseList.data);
	myfree(tmpQueue);
}

















