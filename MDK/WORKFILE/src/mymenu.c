#include "mymenu.h"
#include "esp8266.h"
#define MENUITEMSCNT 5

//状态查看时候LED和KEY图画显示
u8 LED_Image_Light[4*32]=
{
0x00,0x00,0x00,0x00,0x80,0xC0,0xE0,0xF0,
0xF0,0xF8,0xF8,0xFC,0xFC,0xFC,0xFC,0xFC,
0xFC,0xFC,0xFC,0xFC,0xFC,0xF8,0xF8,0xF0,
0xF0,0xE0,0xC0,0x80,0x00,0x00,0x00,0x00,
0x00,0x00,0xF8,0xFE,0xFF,0xFF,0xFF,0xFF,
0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
0xFF,0xFF,0xFF,0xFF,0xFE,0xF8,0x00,0x00,
0x00,0x00,0x1F,0x7F,0xFF,0xFF,0xFF,0xFF,
0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
0xFF,0xFF,0xFF,0xFF,0x7F,0x1F,0x00,0x00,
0x00,0x00,0x00,0x00,0x01,0x07,0x07,0x0F,
0x0F,0x1F,0x1F,0x3F,0x3F,0x3F,0x3F,0x3F,
0x3F,0x3F,0x3F,0x3F,0x3F,0x3F,0x1F,0x1F,
0x0F,0x07,0x03,0x01,0x00,0x00,0x00,0x00
};
u8 LED_Image_Extin[4*32]=
{
0x00,0x00,0x00,0x00,0x80,0xC0,0xE0,0xF0,
0xF0,0x78,0x78,0x3C,0x3C,0x1C,0x1C,0x1C,
0x1C,0x1C,0x1C,0x3C,0x3C,0x78,0x78,0xF0,
0xF0,0xE0,0xC0,0x80,0x00,0x00,0x00,0x00,
0x00,0x00,0xF8,0xFE,0xFF,0x1F,0x07,0x01,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x01,0x07,0x1F,0xFF,0xFE,0xF8,0x00,0x00,
0x00,0x00,0x1F,0x7F,0xFF,0xF8,0xE0,0x80,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x80,0xE0,0xF8,0xFF,0x7F,0x1F,0x00,0x00,
0x00,0x00,0x00,0x00,0x01,0x03,0x07,0x0F,
0x0F,0x1E,0x1E,0x3C,0x3C,0x38,0x38,0x38,
0x38,0x38,0x38,0x3C,0x3C,0x3E,0x1E,0x1F,
0x0F,0x07,0x03,0x01,0x00,0x00,0x00,0x00
};
u8 KEY_Image_Press[4*32]=
{
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x80,0x80,0xC0,
0xC0,0x40,0x40,0x40,0x60,0x60,0x20,0x20,
0x20,0x20,0x60,0x60,0x40,0x40,0x40,0xC0,
0xC0,0x80,0x80,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0xFE,0x0F,0x1F,0x39,0x30,
0x20,0x20,0x60,0x60,0x40,0x40,0x40,0x40,
0x40,0x40,0x40,0x40,0x40,0x60,0x60,0x20,
0x30,0x31,0x1F,0x0F,0xFE,0x00,0x00,0x00,
0x00,0x00,0x00,0x0F,0x18,0x10,0x30,0x20,
0x20,0x20,0x60,0x60,0x40,0x40,0x40,0x40,
0x40,0x40,0x40,0x40,0x60,0x60,0x20,0x20,
0x20,0x30,0x10,0x18,0x0F,0x00,0x00,0x00
};
u8 KEY_Image_Pop[4*32]=
{
0x00,0x00,0x00,0x00,0x80,0xC0,0xC0,0x60,
0x60,0x20,0x20,0x20,0x30,0x30,0x10,0x10,
0x10,0x10,0x30,0x30,0x20,0x20,0x20,0x60,
0x60,0xC0,0xC0,0x80,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0xFF,0x07,0x0F,0x1C,0x18,
0x10,0x10,0x30,0x30,0x20,0x20,0x20,0x20,
0x20,0x20,0x20,0x20,0x20,0x30,0x30,0x10,
0x18,0x18,0x0F,0x07,0xFF,0x00,0x00,0x00,
0x00,0x00,0x00,0xFF,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0xFF,0x00,0x00,0x00,
0x00,0x00,0x00,0x0F,0x18,0x10,0x30,0x20,
0x20,0x20,0x60,0x60,0x40,0x40,0x40,0x40,
0x40,0x40,0x40,0x40,0x60,0x60,0x20,0x20,
0x20,0x30,0x10,0x18,0x0F,0x00,0x00,0x00
};

char blankReplace[20]={'\0','\0','\0'};
bool usartRefreshFlag = true;

menuitem myMenuItem0[MENUITEMSCNT] = 
{
	{"状态", 0xff, 0, ' ', false, 1},
	{"陀螺仪", 0xff, 0, ' ', false, 2},
	{"串口", 0xff, 0, ' ', false, 3},
	{"无线", 0xff, 0, ' ', false, 4},
	{"拓展功能", 0xff, 0, ' ', false, 0}
};
menuitem myMenuItem1[15] = //Status Item
{
	{"LED1  :", 0xfd, 0, ' ', false, 0},
	{"LED2  :", 0xfd, 0, ' ', false, 0},
	{"LED3  :", 0xfd, 0, ' ', false, 0},
	{"LED4  :", 0xfd, 0, ' ', false, 0},
	{"KEY1  :", 0xfd, 0, ' ', false, 0},
	{"KEY2  :", 0xfd, 0, ' ', false, 0},
	{"KEY3  :", 0xfd, 0, ' ', false, 0},
	{"KEY4  :", 0xfd, 0, ' ', false, 0},
	{"AD    :", 0x40, 0, ' ', false, 0},
	{"Acc1  :", 0x50, 0, ' ', false, 0},
	{"Acc2  :", 0x50, 0, ' ', false, 0},
	{"Acc3  :", 0x50, 0, ' ', false, 0},
	{"Gyro1 :", 0x50, 0, ' ', false, 0},
	{"Gyro2 :", 0x50, 0, ' ', false, 0},
	{"Gyro3 :", 0x50, 0, ' ', false, 0},
	
};
menuitem myMenuItem2[9] = //MPU Item
{
	{"Acc1  :", 0x50, 0, ' ', false, 0},
	{"Acc2  :", 0x50, 0, ' ', false, 0},
	{"Acc3  :", 0x50, 0, ' ', false, 0},
	{"Gyro1 :", 0x50, 0, ' ', false, 0},
	{"Gyro2 :", 0x50, 0, ' ', false, 0},
	{"Gyro3 :", 0x50, 0, ' ', false, 0},
	{"Pitch :", 0x31, 0, ' ', false, 0},
	{"Roll  :", 0x31, 0, ' ', false, 0},
	{"Yaw   :", 0x31, 0, ' ', false, 0}
};
menuitem myMenuItem3[2] = //USART Itemc    fc
{
	{"RecCnt :", 0x40, 0, ' ', false, 0},
	{"RecData:", 0xfc, 0, ' ', false, 0}
};
menuitem myMenuItem4[1] = //Wifi Item      fb
{
	{"", 0xfb, 0, ' ', false, 0},
};
menuInfo myMenuSet[5] = 
{
	{myMenuItem0, 0,	0, 5 , 0},
	{myMenuItem1, 0,	0, 15, 1},
	{myMenuItem2, 0,	0, 9 , 2},
	{myMenuItem3, 0,	0, 2 , 3},
	{myMenuItem4, 0,	0, 1 , 4},
};

menucontrol menu =
{
	&myMenuSet[0],
	0,
	true
};


void MenuDisplay(void)
{
	u8 i, j, k;
	s16 tmpNum, tmpCnt;
	static u8 menuPageBack = 255;
	static u8 menuIndexBack = 255;
	static u8 menuSelectBack = 255;
	menuitem *tmpMenuItem;
	pMenuInfo myMenu = menu.pNowMenu;
	myMenu->menuPage = myMenu->menuSelect/4;
	(myMenu->menuPage*4 + 3 < myMenu->selectionCnt) ? (j = 4) : (j = myMenu->selectionCnt - myMenu->menuPage*4);
	if(menuPageBack != myMenu->menuPage || menuIndexBack != myMenu->menuIndex)//翻页 或 打开子菜单时候刷新菜单名
	{
		menu.refreshFlag = false;
		menuPageBack = myMenu->menuPage;
		menuIndexBack = myMenu->menuIndex;
		menuSelectBack = myMenu->menuSelect;
		if(menuIndexBack != 1 || myMenu->menuSelect >= 8)
			My_OLED_Clear();
		if(menuIndexBack == 0 || menuIndexBack == 2)
		{
			OLED_Write_Char(0, menuSelectBack%4*2, '_');
		}
		for(i = 0; i < j; ++ i)
		{
				tmpMenuItem = &myMenu->menuItemSet[i+myMenu->menuPage*4];
				if(menuIndexBack == 0)
					OLED_Write_Chinese_Str(1,2*i,tmpMenuItem->itemName);
				else
					OLED_Write_String(1, 2*i, (u8 *)(tmpMenuItem->itemName));
		}
		
		if(menuIndexBack == 0)
		{
			OLED_Write_Chinese_Str(11,  2*(menuSelectBack%4), "查看");
		}
		if(menuIndexBack == 4)//Wifi菜单显示esp8266的状态(固定字符串)
		{
//			(esp8266Status) ? 
//			(OLED_Write_String(1,0, esp8266SSID)):
//			(OLED_Write_String(1,0, "Not Found Wifi"));
			if(esp8266Status > 0)
			{
				OLED_Write_Num3(1,2,ipStr[0]);OLED_Write_Char(4,2,'.');
				OLED_Write_Num3(5,2,ipStr[1]);OLED_Write_Char(8,2,'.');
				OLED_Write_Num3(9,2,ipStr[2]);OLED_Write_Char(12,2,'.');
				OLED_Write_Num3(13,2,ipStr[3]);
				OLED_Write_Num5(1,4, dtePort);
			}
			myMenuItem4[0].childMenu = 1;
		}
	}
	//菜单数据实时刷新
	if(myMenu->menuItemSet[myMenu->menuSelect].dataType == 0xfd)//key 和 led 八个图画界面处理      
	{
		for(i = 0; i < 4; ++ i)//4个LED灯刷新
		{
			(myMenuItem1[i].flag) ? (OLED_DrawBMP(i*32,0,32,32,LED_Image_Light)) : (OLED_DrawBMP(i*32,0,32,32,LED_Image_Extin));
		}
		for(i = 4; i < 8; ++ i)//4个按键刷新
		{
			(myMenuItem1[i].flag) ? (OLED_DrawBMP((i-4)*32,4,32,32,KEY_Image_Press)) : (OLED_DrawBMP((i-4)*32,4,32,32,KEY_Image_Pop));
		}
	}
	else//普通菜单栏的数值和状态 实时刷新
	{
		for(i = 0; i < j; ++ i)
		{
			tmpMenuItem = &myMenu->menuItemSet[i+myMenu->menuPage*4];
			if(tmpMenuItem->dataType == 0)//开关状态显示
			{
				(tmpMenuItem->flag) ? 
				OLED_Write_String(10,2*i, "True "):
				OLED_Write_String(10, 2*i, "False");
			}
			else if(tmpMenuItem->dataType == 0xff)//子菜单标志Dir显示
			{

			}
			else if(tmpMenuItem->dataType == 0xfe)//LED 和 按键状态显示
			{
				(tmpMenuItem->flag) ? 
				OLED_Write_String(10,2*i, "True "):
				OLED_Write_String(10, 2*i, "False");
			}
			else if(menuIndexBack == 3)//串口页面的处理
			{
				if(myMenuItem3[1].flag == 0)
					ValueDisplay(myMenuItem3, 0);//实时显示统计出来的接收字节数
				if(myMenuItem3[1].flag == 0 && menu.pNowMenu->menuItemSet[0].flag == 1)//串口的第一页 且  需要刷新
				{
						//My_OLED_Clear();
						OLED_Write_String(1, 0, (u8 *)(myMenuItem3[0].itemName));
						OLED_Write_String(1, 2, (u8 *)(myMenuItem3[1].itemName));
						menu.pNowMenu->menuItemSet[0].flag = 0;			
						if(menu.pNowMenu->menuItemSet[1].childMenu)//如果现在是16进制显示状态
						{
							Display_HexLine(10, 2, &USART1_RX_BUF[0],2);
							if(Usart1_Rx_Len > 2)Display_HexLine(1, 4, &USART1_RX_BUF[2],5);
							else	Display_HexLine(1, 4, blankReplace,5);
							if(Usart1_Rx_Len > 7)Display_HexLine(1, 6, &USART1_RX_BUF[7],5);
							else	Display_HexLine(1, 6, blankReplace,5);
						}
						else
						{
							Display_StringLine(10, 2, USART1_RX_BUF,5);
							if(Usart1_Rx_Len > 5)Display_StringLine(1, 4, &USART1_RX_BUF[5],14);
							else	Display_StringLine(1, 4, blankReplace,14);
							if(Usart1_Rx_Len > 19)Display_StringLine(1, 6, &USART1_RX_BUF[19],14);
							else	Display_StringLine(1, 6, blankReplace,14);
						}
				}
				else if(menu.pNowMenu->menuItemSet[0].flag == 1)//刷新标志有效           如果不是第一页 且 需要刷新
				{
					if(menu.pNowMenu->menuItemSet[1].childMenu)//用16进制显示
					{
						tmpNum = menu.pNowMenu->menuItemSet[1].flag;//获取目前所在串口接收的数据的第几页
						tmpNum = tmpNum * 20 - 8;//按照16进制的显示格式计算从第几位数据开始显示
						//My_OLED_Clear();//清屏
						for(k = 0; k < 4; ++ k)
						{
							tmpCnt = tmpNum + k*5;
							if(tmpCnt < Usart1_Rx_Len)
								Display_HexLine(1, 2*k, &USART1_RX_BUF[tmpCnt],5);//有数据就显示到OLED上
							else
								Display_HexLine(1, 2*k, blankReplace,5);//没数据清空当前行
						}
					}
					else//一般字符显示
					{
						tmpNum = menu.pNowMenu->menuItemSet[1].flag;//获取目前所在串口接收的数据的第几页
						tmpNum = tmpNum * 56 - 23;//按照一般字符显示格式计算从第几位数据开始显示
						//My_OLED_Clear();//清屏
						for(k = 0; k < 4; ++ k)
						{
							tmpCnt = tmpNum + k*14;
							if(tmpCnt < Usart1_Rx_Len)
								Display_StringLine(1, 2*k, &USART1_RX_BUF[tmpCnt],14);//有数据就显示到OLED上
							else
								Display_StringLine(1, 2*k, blankReplace, 14);//没数据清空当前行
						}
					}
					menu.pNowMenu->menuItemSet[0].flag = 0;//刷新后清除刷新标志
				}
			}
			else if(menuIndexBack == 4)//wifi模块页面处理
			{
			 if(myMenuItem4[0].flag)
			 {
				// 	(esp8266Status) ? 
					//(OLED_Write_String(1,0, esp8266SSID)):
					//(OLED_Write_String(1,0, "Not Found Wifi"));
					switch(esp8266Status)
					{
						case 1:OLED_Write_String(1,6,"not connected   ");OLED_Write_String(11,0,"    ");break;
						case 2:OLED_Write_String(1,6,"connected       ");OLED_Write_String(11,0,"    ");break;
						case 3:OLED_Write_String(1,6,"connected       ");OLED_Write_String(11,0,"    ");break;
						case 4:OLED_Write_String(10,0,"Pass  ");break;
						default:break;
					}
					if(esp8266Status < 4)
					{
						if(myMenuItem4[0].childMenu == 0)
						{
							OLED_Clear_Line(0,2);
							OLED_Clear_Line(0,4);
						}
						OLED_Write_Num3(1,2,ipStr[0]);OLED_Write_Char(4,2,'.');
						OLED_Write_Num3(5,2,ipStr[1]);OLED_Write_Char(8,2,'.');
						OLED_Write_Num3(9,2,ipStr[2]);OLED_Write_Char(12,2,'.');
						OLED_Write_Num3(13,2,ipStr[3]);
						OLED_Write_Num5(1,4, dtePort);
						OLED_Write_String(10,0," ERR:");
						OLED_Write_Num1(15,0,esp8266Status);
						myMenuItem4[0].childMenu = 1;
					}
					else if(myMenuItem4[0].childMenu)
					{
						OLED_Clear_Line(0,2);
						OLED_Clear_Line(0,4);
						OLED_Clear_Line(0,6);
						OLED_Write_String(1,2,"Rec:");
						myMenuItem4[0].childMenu = 0;
					}
					myMenuItem4[0].flag = 0;	
			 }
			}
			else
				ValueDisplay(tmpMenuItem, 2*i);
		}
	}
	if(menuSelectBack != myMenu->menuSelect || myMenu->selectionCnt == 1)//菜单箭头实时刷新
	{
		menuSelectBack = myMenu->menuSelect;
		for(i = 0; i < 4; ++ i)
		{
			OLED_Write_Char(0, 2*i, ' ');
			if(menuIndexBack == 0)
				Display_StringLine(10, 2*i, blankReplace,14);
		}
		if(menuIndexBack!= 1 && menuIndexBack!= 3 && menuIndexBack != 4)
		{
			OLED_Write_Char(0, 2*(menuSelectBack%4), '_');
			if(menuIndexBack == 0)
				OLED_Write_Chinese_Str(11,  2*(menuSelectBack%4), "查看");
		}
	}
}

//留6位用于显示数值
void ValueDisplay(menuitem * tmpItem, u8 row)
{
	float tmpNum = tmpItem->value;
	u8 i = 0, n = (tmpItem->dataType) >> 4, m;
	s32 num = tmpNum;
	m = n+10;
	if(num < 0)
		OLED_Write_Char(9, row, '-');
	else
		OLED_Write_Char(9, row, ' ');
	if(tmpItem->dataType & 0x0F)
		OLED_Write_Char(m++, row, '.');
	while(n--)
	{
		OLED_Write_Num1(10+n, row, num%10);
		num /= 10;
	}
	n = tmpItem->dataType & 0x0F;
	if(n <= 0)
		return;
	for(i = 0; i < n; ++ i) 
	{
		tmpNum *= 10;
		num = tmpNum;
		OLED_Write_Num1(m+i, row, num%10);
	}
}

//用于在某行显示字符
void Display_StringLine(u8 x, u8 y, char * tmpChar, u8 num)
{
	u8 i,tmpNum = strlen(tmpChar), rowCnt = x;
	if(tmpNum > num)//防止溢出
		tmpNum = num;
	for (i=0; i < tmpNum; i++)
	{
		OLED_Write_Char(rowCnt++, y, tmpChar[i]);
	}
	while(rowCnt < 16)
			OLED_Write_Char(rowCnt++, y, ' ');
}

char NumToHexChar(u8 num)
{
	if(num <= 9)
		return num+'0';
	else 
		return num-10+'A';
}

void Display_HexLine(u8 x, u8 y, char * tmpChar, u8 num)
{
	u8 i,tmpNum = strlen(tmpChar), rowCnt = x;
	if(tmpNum > num)//防止溢出
		tmpNum = num;
	for (i=0; i < tmpNum; i++)
	{
		OLED_Write_Char(rowCnt++, y, NumToHexChar(tmpChar[i]/16));
		OLED_Write_Char(rowCnt++, y, NumToHexChar(tmpChar[i]%16));
		OLED_Write_Char(rowCnt++, y, ' ');
	}
	while(rowCnt < 16)
			OLED_Write_Char(rowCnt++, y, ' ');
}










