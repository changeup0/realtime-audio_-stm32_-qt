#include "datasend.h"
#include "header.h"
//led状态记录
ledsta ledStatus = {1,1,1,1};
//可选择    Usart1_Send   \    HC05_Send
void (*Data_Send_Usart)(u8 *, bool, bool) = Usart1_Send;
//void (*infoSend)(u8 *, bool, bool);
//MPU6050   传输源数据
void Trans_SrcData(void)
{
	char tempStr[180];
	sprintf(tempStr, "Y,%d,%d,%d,%d,%d,%d,%d", Get_Adc(1), accel[0], accel[1],accel[2],gyro[0],gyro[1],gyro[2]);
	Data_Send_Usart((u8 *)tempStr, true, false);
}

//MPU6050  传输处理后的数据
void Trans_DstData(void)
{
	char tempStr[180];
	sprintf(tempStr, "R,%.1f,%.1f,%.1f,%.1f", (float)Get_Adc(1)*3.3/4096, Pitch, Roll, Yaw);
	Data_Send_Usart((u8 *)tempStr, true, false);
}

//传输led和按键状态
void Trans_Key_Led_Sta()
{
	char tempStr[50];
	tempStr[0] = 'B';tempStr[1] = 'D';tempStr[2] = ',';
	(ledStatus.led1) ? (tempStr[3] = '1') : (tempStr[3] = '0');
	(ledStatus.led2) ? (tempStr[4] = '1') : (tempStr[4] = '0');
	(ledStatus.led3) ? (tempStr[5] = '1') : (tempStr[5] = '0');
	(ledStatus.led4) ? (tempStr[6] = '1') : (tempStr[6] = '0');
	tempStr[7] = ',';
	(key1.avaiCnt) ? (tempStr[8] = 'D') : (tempStr[8] = 'U');
	(key2.avaiCnt) ? (tempStr[9] = 'D') : (tempStr[9] = 'U');
	(key3.avaiCnt) ? (tempStr[10] = 'D') : (tempStr[10] = 'U');
	(key4.avaiCnt) ? (tempStr[11] = 'D') : (tempStr[11] = 'U');
	tempStr[12] = '\0';
	Data_Send_Usart((u8 *)tempStr, true, false);
}









