#include "mystring.h"
#include "header.h"
#include "string.h"

u16 FindArray(u16 beginIndex, char c, char * srcArray)
{
	u16 len = strlen(srcArray);u16 back = len;
	char *ptr;
	if(len <= beginIndex)
		return NOT_FIND;
	ptr = srcArray + beginIndex;
	while(len && *ptr != c){ptr++;len--;}
	if(len == 0)	return NOT_FIND;
	else	return back-len;
}

u16 CountArray(char c, char * srcArray)
{
	u16 len = strlen(srcArray), cnt = 0;
	char *ptr = srcArray;
	while(len--)
			if(*ptr++ == c)
				cnt++;
	return cnt;
}


