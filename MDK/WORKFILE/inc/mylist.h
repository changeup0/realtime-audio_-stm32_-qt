#ifndef __MYLIST_H
#define __MYLIST_H
#include "sys.h"
/********************该文件模拟了线性表、堆栈以及队列*******************/


//该线性表利用数组实现，使用时注意其数据类型
//函数从上到下有调用关系 修改时注意  
//只支持8位
/*因为单片机中尽量不要使用malloc和free  因此这样使用mylist 如下
在创建函数中：
							输入：需要的数据类型和预估数据数量
							输出：一个同输入数据类型*ptr的指针
							使用：ptr指针即为新表
*/
typedef u8 bool;
//基本线性表
typedef struct MYLIST
{
	u8 * data;
	u16 dataCnt;
	u16 maxLen;
}myList, * mylist;
//堆栈
typedef struct MYSTACK
{
	myList baseList;
	u8 (*top)(struct MYSTACK*);
	u16 (*size)(struct MYSTACK*);
	bool (*empty)(struct MYSTACK*);
	void (*push)(struct MYSTACK*, u8);
	void (*pop)(struct MYSTACK*);
	void (*destroy)(struct MYSTACK*);
}mystack, *pmystack;
//队列
typedef struct MYQUEUE
{
	myList baseList;
	u8 queueTopIndex;
	u8 queueTailIndex;
	u8 (*front)(struct MYQUEUE * tmpQueue);
	u16 (*size)(struct MYQUEUE * tmpQueue);
	bool (*empty)(struct MYQUEUE * tmpQueue);
	void (*push)(struct MYQUEUE * tmpQueue, u8 value);
	void (*pop)(struct MYQUEUE * tmpQueue);
	void (*destroy)(struct MYQUEUE * tmpQueue);
}myqueue, *pmyqueue;
//创建
mylist MyList_Creat(u16 len);
//删除
void MyList_Free(mylist SqList);
//查找数据
u16 MyList_FindData(mylist SqList, u16 data);
//查找下标
u16 MyList_FindIndex(mylist SqList, u16 index);
//插入
bool MyList_Insert(mylist SqList, u8 data, u16 index);
//删除数据
bool MyList_Delete(mylist SqList, u16 index);
//尾部插入
bool MyList_PushBack(mylist SqList, u8 data);
//尾部弹出
void MyList_PopBack(mylist SqList);
//查看长度
u16 MyList_Size(mylist SqList);
//保留部分数据
bool MyList_Reserve(mylist SqList, u16 len);

//模拟堆栈
pmystack MyStack_Creat(u16 len);
u8 MyStack_top(pmystack tmpStack);
u16 MyStack_Size(pmystack tmpStack);
bool MyStack_Empty(pmystack tmpStack);
void MyStack_Push(pmystack tmpStack, u8 value);
void MyStack_Pop(pmystack tmpStack);
void MyStack_Delete(pmystack tmpStack);
//模拟队列(注意初始化时容量要略大于所需  不然会出错  push没有考虑这个问题)
pmyqueue MyQueue_Creat(u16 len);
u8 MyQueue_Front(struct MYQUEUE * tmpQueue);
u16 MyQueue_Size(struct MYQUEUE * tmpQueue);
bool MyQueue_Empty(struct MYQUEUE * tmpQueue);
void MyQueue_Push(struct MYQUEUE * tmpQueue, u8 value);
void MyQueue_Pop(struct MYQUEUE * tmpQueue);
void MyQueue_Delete(struct MYQUEUE * tmpQueue);
#endif
