#ifndef __MYMENU_H
#define __MYMENU_H

#include "sys.h"
#include "header.h"

typedef struct MENUITEM
{
	char itemName[20];//菜单项名字
	const u8 dataType;//高四位表示显示整数位数  低四位表示小数位数  0x00开关  
	
	float value;//菜单项值     dataType > 0x00 && dataType < 0xff时候使用这个
	char itemUnitStr;//菜单项单位
	
	bool flag;//菜单项值	     dataType == 0x00时候使用这个
	
	u8 childMenu;//子菜单      dataType == 0xff时候使用这个
}menuitem;

typedef struct MENUINFO
{
	menuitem * menuItemSet;
	u8 menuPage;
	u8 menuSelect;
	const u8 selectionCnt;
	const u8 menuIndex;
}menuInfo, * pMenuInfo;

typedef struct MENUCONTROL
{
	pMenuInfo pNowMenu;
	u8 lastMenue;
	bool refreshFlag;
}menucontrol;




extern menucontrol menu;
extern menuInfo myMenuSet[];
extern menuitem myMenuItem1[];
extern menuitem myMenuItem2[];
extern menuitem myMenuItem3[];
extern menuitem myMenuItem4[];
extern char blankReplace[];

void MenuDisplay(void);
void ValueDisplay(menuitem * tmpItem, u8 row);
void Display_StringLine(u8 x, u8 y, char * tmpChar, u8 num);
void Display_HexLine(u8 x, u8 y, char * tmpChar, u8 num);

#endif
