#ifndef __MYMALLOC_H
#define __MYMALLOC_H
#include "header.h"
//根据正点原子库修改
//采用首次适应算法 以及 位示图法
#define MEM_BLOCK_SIZE 32          //内存块大小为32字节
#define MAX_MEM_SIZE   10*1024       //最大管理内存 10K
#define MEM_ALLOC_TABLE_SIZE (MAX_MEM_SIZE/MEM_BLOCK_SIZE) //内存表大小

//内存管理控制器
typedef struct _m_mallco_dev
{
 void (*init)(void);     //初始化
 u8 (*perused)(void);         //内存使用率
 u8  membase[MAX_MEM_SIZE];   //内存池
 u16 memmap[MEM_ALLOC_TABLE_SIZE];  //内存管理状态表
 u8  memrdy;        //内存管理是否就绪
}software_mmdev;


//定义内存管理控制器；
extern software_mmdev malloc_dev;
//内部调用函数
u32 mem_malloc(u32 size);     //内存分配
u8 mem_free(u32 offset);     //内存释放
u8 mem_perused(void);      //获得内存使用率

//外部调用函数
void mem_init(void);      //内存管理初始化函数(外/内部调用)
void myfree(void *ptr);       //内存释放(外部调用)
void *mymalloc(u32 size);     //内存分配(外部调用)
void *myrealloc(void *ptr,u32 size);  //重新分配内存(外部调用)
  
#endif



