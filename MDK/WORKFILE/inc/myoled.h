
#ifndef __MYOLED_H
#define __MYOLED_H

#include "sys.h"
//#include "extern.h"
//#include "data.h"


/****************/
/*
OLED使用说明：
指定坐标显示 String 或 number 直接调用myoled中的函数
若需要进行精细操作  可以通过oled库 修改OLED_GRAM数组  然后调用OLED_Refresh_Gram()刷新就行
*/
/******************** Device Config ********************/

/* OLED端口宏定义 */
#define OLED_CLK PBout(10)
#define OLED_SDA_W PBout(11)
#define OLED_SDA_R PBin(11)

#define Font   1//1是大字体 0是小字体 小字体比较省空间


/* OLED分辨率宏定义 */
#define  X_WIDTH  128
#define  Y_WIDTH  64


/* OLED IO输出宏替换 */
//IO方向设置
#define OLED_SDA_IN()  {GPIOB->CRH&=0XFFFF0FFF;GPIOB->CRH|=8<<12;}
#define OLED_SDA_OUT() {GPIOB->CRH&=0XFFFF0FFF;GPIOB->CRH|=3<<12;}

//IO操作函数	 
#define OLED_IIC_SCL    PBout(10) //SCL
#define OLED_IIC_SDA    PBout(11) //SDA	 
#define OLED_READ_SDA   PBin(11)  //输入SDA 

//IIC所有操作函数
void OLED_IIC_Init(void);                //初始化IIC的IO口				 
int OLED_IIC_Start(void);				//发送IIC开始信号
void OLED_IIC_Stop(void);	  			//发送IIC停止信号
void OLED_IIC_Send_Byte(u8 txd);			//IIC发送一个字节
u8 OLED_IIC_Read_Byte(unsigned char ack);//IIC读取一个字节
int OLED_IIC_Wait_Ack(void); 				//IIC等待ACK信号
void OLED_IIC_Ack(void);					//IIC发送ACK信号
void OLED_IIC_NAck(void);				//IIC不发送ACK信号


void OLED_WrDat(uint8_t data);

void OLED_WrCmd(uint8_t cmd);

void OLED_Set_XY(uint8_t x, uint8_t y);

void My_OLED_Clear(void);

//延时函数
void OLED_Delay_ms(uint16_t ms);

void My_OLED_Init(void);        
     
void OLED_Write_Char(uint8_t x, uint8_t y, uint8_t c);

void OLED_Write_String(uint8_t x, uint8_t y, uint8_t *s);

void OLED_Write_Num1(uint8_t x, uint8_t y, uint8_t num);

void OLED_Write_Num2(uint8_t x, uint8_t y, uint8_t num);

void OLED_Write_Num3(uint8_t x, uint8_t y, uint16_t num);

void OLED_Write_Num4(uint8_t x, uint8_t y, uint16_t num);

void OLED_Write_Num5(uint8_t x, uint8_t y, uint16_t num);

void OLED_Write_Value(uint8_t x, uint8_t y, uint16_t num,uint8_t Type);

void OLED_DrawBMP(u8 x0, u8 y0,u8 x1, u8 y1,u8 BMP[]);

void OLED_Clear_Line(u8 x, u8 y);
//void OLED_DrawChinese(u8 x0, u8 y0,u8 x1, u8 y1, u8 line, u8 BMP[]);//画汉字 哈哈哈哈
#endif

