#ifndef __CMDDEAL_H
#define __CMDDEAL_H
#include "header.h"
//！！！！！！！！！！！！！！！！！
/*默认输入指令关键字和参数个数相同的情况下，参数不符合需求*/

#define LED_Water_TypeCnt 3

extern bool CmdMode_EN;
extern bool Usart1_CmdMode_EN;
extern bool Usart2_CmdMode_EN;


extern u8 ledWaterType;
extern u8 ledWaterSpeed;
extern u32 ledWaterTime;
extern u8 ledWaterSta;
//一个指令应该包括的内容:	关键字   参数  参数个数(帮助处理)   执行函数
typedef struct CMD
{
	const u8 * keyStr;
	const u8 * cmdformat;
	const u8 paramCnt;
	const u8 id;
	void * func;
}cmd;

typedef struct CMDSET
{
	cmd * cmdList;
	void (*infoSend)(u8 *, bool, bool);
	u32 (*exec)(cmd);
	
}cmdset;

extern cmdset myCmdSet; 

void Cmd_detect_Usart1(u8 sta);
void Cmd_detect_Usart2(u8 sta);
u8 Cmd_Scan(u8 * CmdStr);
u32 Cmd_Exec(cmd command);
void Param_Get(u8 * tmpCmdStr);

void Cmd_Help(void);
void LED_Light(u8 n);
void LED_PutOut(u8 n);
void LED_Control(u8 * sParam);
void LED_WaterType(u8);
void LED_WaterSpeed(u8);
void LED_WaterBrightness(u8);
void DATA_Trans(u8 *tParam, u8 type);
void LED_WaterControl(u8 * sParam);

void Cmd_WorkState(void);
void Cmd_delay_test(u8 * pT);





#endif
