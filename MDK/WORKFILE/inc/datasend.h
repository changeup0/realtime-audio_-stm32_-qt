#ifndef __DATASEND_H
#define __DATASEND_H
#include "header.h"

typedef struct LEDSTA
{
	bool led1,led2,led3,led4;
}ledsta;

extern ledsta ledStatus;

extern void (*Data_Send_Usart)(u8 *, bool, bool);
	

//传输源数据
void Trans_SrcData(void);
//传输处理后的数据
void Trans_DstData(void);
//传输LED和KEY的状态
void Trans_Key_Led_Sta(void);






#endif
