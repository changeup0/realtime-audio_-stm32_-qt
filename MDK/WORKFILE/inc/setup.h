#ifndef __SETUP_H
#define __SETUP_H
#include "sys.h"
typedef u8 bool;


extern bool LED1_PWM_EN;
extern bool LED2_PWM_EN;
extern bool LED3_PWM_EN;
extern bool LED4_PWM_EN;

extern bool USART1_EN;
extern bool USART2_EN;
extern bool USART3_EN;

extern bool MPU6050_EN;

extern bool KEY_EN;

void TIM3_IRQHandler(void);

#endif


