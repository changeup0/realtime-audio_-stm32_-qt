#ifndef __MYKEY_H
#define __MYKEY_H		
#include "header.h"
//用定时中断扫描按键
//调整按键设置时候需要注意：1.开关 2.引脚 3.初始化中的端口时钟配置 4.上拉输入和下拉输入

#define key1Switch 1
#define key2Switch 1
#define key3Switch 1
#define key4Switch 1

#if key1Switch
#define KEY1_Port GPIOA
#define KEY1_Pin GPIO_Pin_4
#define KEY1_Status GPIO_ReadInputDataBit(KEY1_Port,KEY1_Pin)
#endif

#if key2Switch
#define KEY2_Port GPIOA
#define KEY2_Pin GPIO_Pin_5
#define KEY2_Status GPIO_ReadInputDataBit(KEY2_Port,KEY2_Pin)
#endif

#if key3Switch
#define KEY3_Port GPIOA
#define KEY3_Pin GPIO_Pin_8
#define KEY3_Status GPIO_ReadInputDataBit(KEY3_Port,KEY3_Pin)
#endif

#if key4Switch
#define KEY4_Port GPIOA
#define KEY4_Pin GPIO_Pin_15
#define KEY4_Status GPIO_ReadInputDataBit(KEY4_Port,KEY4_Pin)
#endif

typedef struct MYKEY
{
	u16 avaiCnt;
	u8 pressCnt;
	u16 avaiCntMax;
}mykey;
//引用到外部
#if key1Switch
extern mykey key1;
#endif

#if key2Switch
extern mykey key2;
#endif

#if key3Switch
extern mykey key3;
#endif

#if key4Switch
extern mykey key4;
#endif




//支持函数调用
void MyKeyInit(void);

	 

#endif

