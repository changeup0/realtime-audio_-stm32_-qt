#ifndef __MYPWM_H
#define __MYPWM_H		
#include "header.h"
//用定时中断模拟PWM
//此处结构体用于定义新的pwm   记得初始化（可以仿照.c中的例子）
//调整pwm设置时候需要注意：1.pwm开关 2.引脚 3.状态 4.初始化中的端口时钟配置


//[开发板只有两个LED，为了开发板测试方便增加一个开关，^_^]
//这个变量为1 则四个模拟pwm自动绑定到led上  [其他地方不需要修改，所有设置自动跟随LED文件]
#define pwmLed 1
//
#define pwm1Switch 1
#define pwm2Switch 1
#define pwm3Switch 1
#define pwm4Switch 1

#if pwmLed

#if led1Switch
#define PWM1_Status LED1	
#endif

#if led2Switch
#define PWM2_Status LED2	
#endif

#if led3Switch
#define PWM3_Status LED3
#endif

#if led4Switch
#define PWM4_Status LED4
#endif

#else

#if pwm1Switch
#define PWM1_Port GPIOB
#define PWM1_Pin GPIO_Pin_4
#define PWM1_Status PBout(4)	
#endif

#if pwm2Switch
#define PWM2_Port GPIOB
#define PWM2_Pin GPIO_Pin_5
#define PWM2_Status PBout(5)	
#endif

#if pwm3Switch
#define PWM3_Port GPIOB
#define PWM3_Pin GPIO_Pin_6
#define PWM3_Status PBout(6)
#endif

#if pwm4Switch
#define PWM4_Port GPIOB
#define PWM4_Pin GPIO_Pin_7
#define PWM4_Status PBout(7)
#endif

#endif





typedef struct MYPWM
{
	u8 freCnt;
	u8 freCntMax;
	u8 duty;
}mypwm;
//引用到外部
#if pwm1Switch
extern mypwm pwm1;
#endif

#if pwm2Switch
extern mypwm pwm2;
#endif

#if pwm3Switch
extern mypwm pwm3;
#endif

#if pwm4Switch
extern mypwm pwm4;
#endif




//支持函数调用
void MyPwmInit(void);
void MyPwmSetDuty(u8 n, u8 duty);//duty 0-9  原本是0-39  为了亮度明显 取低27 在函数内进行乘2处理

#endif  
	 



