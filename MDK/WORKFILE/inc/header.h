#ifndef __HEADER_H
#define __HEADER_H		
//所需要包含的头文件
#include "sys.h"
#include "setup.h"
#include "led.h"
#include "math.h"
#include "delay.h"
#include "myoled.h"
#include "led.h" 
#include "usart.h"
#include "boot.h"
#include "string.h"
#include "timer.h"
#include "adc.h"
#include "iic.h"
#include "dmpmap.h"
#include "dmpkey.h"
#include "inv_mpu.h"
#include "inv_mpu_dmp_motion_driver.h"
#include "MPU6050.h"
#include "esp8266.h"
#include "mydisplay.h"
#include "mypwm.h"
#include "mykey.h"
#include "mystring.h"
#include "cmddeal.h"
#include "datasend.h"
#include "mymenu.h"
#include "myfront.h"
#include "wm8978.h"

typedef u8 bool;


//定义一些状态
#define true 1
#define false 0
#define NULL 0

#define ERROR_LINE 0x7ff0 //ERROR判断条件
#define NOT_FIND 0x7fff
#define NOT_CUT 0x7ffe
#define ERROR_QueuePush 0x7ffd

#define CMD_ERROR 0xff
#define PARAM_ERROR 0xfe
#define EXEC_OK 0xfd
#define CMD_NOT 0xfc



//系统工作时间
#define clkS clkCnt/1000
#define clkM clkCnt/60000
#define clkH clkCnt/3600000


//一些全局变量
extern u32 clkCnt;
extern u16 adcSrcValue;
extern u8 mpuControlFlag;
extern u8 mpuVisualFlag;
extern bool configFlag;
extern bool usartRefreshFlag;
extern const u16 cu_audioFrameLen;
extern u16 ia_audioBuf1[], ia_audioBuf2[];
extern bool b_buf1Flag, b_buf2Flag;
//一些开关
#define USART1_Frame false
#define USART2_Frame false
// 音频帧采集计数
extern u32 getFrameCnt;

//主函数中的一些处理
void USART1_Deal(void);
void USART2_Deal(void);
void Key_Deal(void);

#endif  
	 



