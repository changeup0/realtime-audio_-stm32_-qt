#ifndef __USART_H
#define __USART_H
#include "stdio.h"	
#include "sys.h" 
#include "header.h"
/*****  目前写了两个usart的函数  需要更多  模仿添加（注意修改关键地方）
USART1 : TX  A9   RX  A8
USART2 : TX  A2   RX  A3
*****/
typedef u8 bool;
//使用hc05(使用的usart2)的时候，注释掉此处的usart2    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#define usart2_switch 1

#define USART_REC_LEN  			200  	//定义最大接收字节数 200

#define EN_USART1_RX 			1		//使能（1）/禁止（0）串口1接收    
extern char  USART1_RX_BUF[USART_REC_LEN]; //接收缓冲,最大USART_REC_LEN个字节.末字节为换行符 
extern u8  USART1_TX_BUF[USART_REC_LEN];
extern u16 USART1_RX_STA;         		//接收状态标记	

//定义帧头帧尾尾数
#define frame1HeadLen 2
#define frame1TailLen 1
//在.c中设置帧头帧尾
extern char frame1Head[];
extern char frame1Tail[];


extern char frame2Head[];
extern char frame2Tail[];

extern char  USART2_RX_BUF[USART_REC_LEN]; 		//接收缓冲,最大USART2_MAX_RECV_LEN字节
extern u8  USART2_TX_BUF[USART_REC_LEN]; 		//发送缓冲,最大USART2_MAX_SEND_LEN字节
extern u16 USART2_RX_STA;   						//接收数据状态


extern u32 Usart2_Send_Cnt;
extern u32 Usart2_Rec_Cnt;
extern s16 Usart2_Rx_Len;
extern u32 Usart2_Rec_Ing;
extern bool startTrans2Flag;
extern u8 frame2HeadCmpCnt;

void Usart2_init(u32 bound);
void Usart2_Send(u8 * sendStr, bool packFlag, bool printLn);
void Usart2_test(void);
void Usart1_init(u32 bound);
void Usart1_Send(u8 * sendStr, bool packFlag, bool printLn);//要发送的字符串  是否打包成帧  是否发送换行
void Usart1_test(void);

extern u32 Usart1_Send_Cnt;
extern u32 Usart1_Rec_Cnt;
extern s16 Usart1_Rx_Len;
extern u32 Usart1_Rec_Ing;
extern bool startTrans1Flag;
extern u8 frame1HeadCmpCnt;
//注意destStr的容量起码要保证容纳的下原数据和帧头帧尾
extern bool startTrans1Flag;
void Usart_Pack(u8 * sendStr, u8 * destStr);//此函数输入时要带有字符串结束符号   打包后会有

extern u8 waitACK;


//#if usart2_switch
#define EN_USART2_RX 			1		//使能（1）/禁止（0）串口1接收 
//extern char  USART2_RX_BUF[USART_REC_LEN]; //接收缓冲,最大USART_REC_LEN个字节.末字节为换行符 
//extern u16 USART2_RX_STA;

//#endif


#endif


