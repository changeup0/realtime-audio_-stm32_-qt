#include "sys.h"
#include "usart.h"
#include "header.h"
#include "string.h"
//////////////////////////////////////////////////////////////////////////////////
//如果使用ucos,则包括下面的头文件即可.
#if SYSTEM_SUPPORT_OS
#include "includes.h" //ucos 使用
#endif
//////////////////////////////////////////////////////////////////////////////////
//本程序只供学习使用，未经作者许可，不得用于其它任何用途
//ALIENTEK STM32开发板
//串口1初始化
//正点原子@ALIENTEK
//技术论坛:www.openedv.com
//修改日期:2012/8/18
//版本：V1.5
//版权所有，盗版必究。
//Copyright(C) 广州市星翼电子科技有限公司 2009-2019
//All rights reserved
//********************************************************************************
//V1.3修改说明
//支持适应不同频率下的串口波特率设置.
//加入了对printf的支持
//增加了串口接收命令功能.
//修正了printf第一个字符丢失的bug
//V1.4修改说明
//1,修改串口初始化IO的bug
//2,修改了USART_RX_STA,使得串口最大接收字节数为2的14次方
//3,增加了USART_REC_LEN,用于定义串口最大允许接收的字节数(不大于2的14次方)
//4,修改了EN_USART1_RX的使能方式
//V1.5修改说明
//1,增加了对UCOSII的支持
//////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////
//加入以下代码,支持printf函数,而不需要选择use MicroLIB
#if 1
#pragma import(__use_no_semihosting)
//标准库需要的支持函数
struct __FILE
{
	int handle;
};

FILE __stdout;
//定义_sys_exit()以避免使用半主机模式
_sys_exit(int x)
{
	x = x;
}
//重定义fputc函数
int fputc(int ch, FILE *f)
{
	while ((USART1->SR & 0X40) == 0)
		; //循环发送,直到发送完毕
	USART1->DR = (u8)ch;
	return ch;
}
#endif

char frame1Head[2] = {'7', 'M'};
char frame1Tail[1] = {'\n'};
//打包到新数组
void Usart_Pack(u8 *sendStr, u8 *destStr)
{
	u16 len = frame1HeadLen, lenBack;
	u8 i, tmp = 0, tmpA, tmpB;
	char *ptr = frame1Head;
	u8 *tmpPtr = destStr;
	while (len--)
		*destStr++ = *ptr++;
	len = strlen((const char *)sendStr);
	lenBack = len + frame1HeadLen;
	//	 if(len > USART2_MAX_SEND_LEN)
	//		 len = USART2_MAX_SEND_LEN-frame1TailLen;
	while (len--)
		*destStr++ = *sendStr++;
	for (i = 0; i < lenBack; ++i)
	{
		tmp += *tmpPtr++;
	}
	(tmp / 16 < 10) ? (tmpA = tmp / 16 + '0') : (tmpA = tmp / 16 - 10 + 'A');
	(tmp % 16 < 10) ? (tmpB = tmp % 16 + '0') : (tmpB = tmp % 16 - 10 + 'A');
	*destStr++ = tmpA;
	*destStr++ = tmpB;
	len = frame1TailLen;
	ptr = frame1Tail;
	while (len--)
		*destStr++ = *ptr++;
	//	 *destStr++ = '\n';
	*destStr = '\0';
}

#if EN_USART1_RX //如果使能了接收
//串口1中断服务程序
//注意,读取USARTx->SR能避免莫名其妙的错误
char USART1_RX_BUF[USART_REC_LEN] = {'\0'}; //接收缓冲,最大USART_REC_LEN个字节.
u8 USART1_TX_BUF[USART_REC_LEN];
//接收状态
//bit15，	接收完成标志
//bit14，	接收到0x0d
//bit13~0，	接收到的有效字节数目
u16 USART1_RX_STA = 0;		  //接收状态标记
bool startTrans1Flag = false; //接收开启标记
u8 frame1HeadCmpCnt = 0;
u32 Usart1_Send_Cnt = 0;
u32 Usart1_Rec_Cnt = 0;
s16 Usart1_Rx_Len = 0;
u32 Usart1_Rec_Ing = 0;
void Usart1_init(u32 bound)
{
	//GPIO端口设置
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1 | RCC_APB2Periph_GPIOA, ENABLE); //使能USART1，GPIOA时钟
	USART_DeInit(USART1);
	//USART1_TX   GPIOA.9
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9; //PA.9
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP; //复用推挽输出
	GPIO_Init(GPIOA, &GPIO_InitStructure);			//初始化GPIOA.9

	//USART1_RX	  GPIOA.10初始化
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;			  //PA10
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING; //浮空输入
	GPIO_Init(GPIOA, &GPIO_InitStructure);				  //初始化GPIOA.10

	//Usart1 NVIC 配置
	NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 3; //抢占优先级3
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;		  //子优先级3
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;			  //IRQ通道使能
	NVIC_Init(&NVIC_InitStructure);							  //根据指定的参数初始化VIC寄存器

	//USART 初始化设置

	USART_InitStructure.USART_BaudRate = bound;										//串口波特率
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;						//字长为8位数据格式
	USART_InitStructure.USART_StopBits = USART_StopBits_1;							//一个停止位
	USART_InitStructure.USART_Parity = USART_Parity_No;								//无奇偶校验位
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None; //无硬件数据流控制
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;					//收发模式

	USART_Init(USART1, &USART_InitStructure);	   //初始化串口1
	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE); //开启串口接受中断
	USART_Cmd(USART1, ENABLE);					   //使能串口1
}

void USART1_IRQHandler(void) //串口1中断服务程序
{
	u8 Res, tmp = 0, tmpA, tmpB, i;
#if SYSTEM_SUPPORT_OS //如果SYSTEM_SUPPORT_OS为真，则需要支持OS.
	OSIntEnter();
#endif
	if (USART_GetITStatus(USART1, USART_IT_RXNE) != RESET) //接收中断(接收到的数据必须是0x0d 0x0a结尾)
	{
		Res = USART_ReceiveData(USART1); //读取接收到的数据
#if USART1_Frame
		if (frame1HeadCmpCnt < frame1HeadLen)
			(Res == frame1Head[frame1HeadCmpCnt]) ? (frame1HeadCmpCnt++) : (frame1HeadCmpCnt = 0);
#endif
		//if(!USART1_Frame ||startTrans1Flag)
		if ((USART1_RX_STA & 0x8000) == 0) //接收未完成
		{
			//	Usart1_Rec_Ing = clkCnt;
			if (Res == '\n')
			{
				// "\n\r"或"\n"均为结束符
				if((USART1_RX_STA & 0X3FFF) == 1)
					USART1_RX_STA = 0;
				if ((USART1_RX_STA & 0X3FFF) - 1 >= 0 && USART1_RX_BUF[(USART1_RX_STA & 0X3FFF) - 1] == '\r')
				{
					USART1_RX_BUF[(USART1_RX_STA & 0X3FFF) - 1] = '\0';
					USART1_RX_STA--;
					USART1_RX_STA |= 0x8000;
				}
				else{
					USART1_RX_BUF[USART1_RX_STA & 0X3FFF] = '\0';
					USART1_RX_STA |= 0x8000;
				}
				
#if USART1_Frame
				USART1_RX_STA -= 2;
				for (i = 0; i < frame1HeadLen; ++i)
					tmp += frame1Head[i];
				for (i = 0; i < USART1_RX_STA; ++i)
					tmp += USART1_RX_BUF[i];
				(tmp / 16 < 10) ? (tmpA = tmp / 16 + '0') : (tmpA = tmp / 16 - 10 + 'A');
				(tmp % 16 < 10) ? (tmpB = tmp % 16 + '0') : (tmpB = tmp % 16 - 10 + 'A');

				if ((!USART1_Frame) || (USART1_RX_BUF[USART1_RX_STA] == tmpA && USART1_RX_BUF[USART1_RX_STA + 1] == tmpB))
				{
					if (USART1_Frame)
						USART1_RX_BUF[USART1_RX_STA] = '\0';
					Usart1_Rx_Len = USART1_RX_STA;
					Usart1_Rec_Cnt += USART1_RX_STA;
					USART1_RX_STA |= 0x8000;
					myMenuItem3[0].flag = true;
					frame1HeadCmpCnt = 0;
					startTrans1Flag = false;
				}
				else
				{
					USART1_RX_STA = 0;
					USART1_RX_BUF[0] = '\0';
					frame1HeadCmpCnt = 0;
					startTrans1Flag = false;
				}
#endif
			}
			else
			{
				USART1_RX_BUF[USART1_RX_STA & 0X3FFF] = Res;
				USART1_RX_STA++;
				//USART1_RX_BUF[USART1_RX_STA&0X3FFF]='\0';
				if (USART1_RX_STA > (USART_REC_LEN - 1))
				{
					USART1_RX_STA = 0;
#if USART1_Frame
					frame1HeadCmpCnt = 0;
					startTrans1Flag = false;
#endif
				} //接收数据错误,重新开始接收
			}

#if USART1_Frame
			if (frame1HeadCmpCnt == frame1HeadLen)
			{
				frame1HeadCmpCnt++;
				startTrans1Flag = true;
			}
#endif
		}
	}
#if SYSTEM_SUPPORT_OS //如果SYSTEM_SUPPORT_OS为真，则需要支持OS.
	OSIntExit();
#endif
}

void Usart1_test()
{
	uint16_t times = 0;
	while (1)
	{
		if (USART1_RX_STA & 0x8000)
		{
			printf("\r\n您发送的消息为:\r\n");
			Usart1_Send((u8 *)USART1_RX_BUF, false, true);
			printf("\r\n"); //插入换行
			USART1_RX_STA = 0;
		}
		else
		{
			times++;
			if (times % 5000 == 0)
			{
				printf("\r\nALIENTEK MiniSTM32开发板 串口实验\r\n");
				printf("正点原子@ALIENTEK\r\n\r\n\r\n");
			}
			if (times % 200 == 0)
				printf("请输入数据,以回车键结束\r\n");
			//			if(times%30==0)LED2=!LED2;//闪烁LED,提示系统正在运行.
			delay_ms(10);
		}
	}
}
void Usart1_Send(u8 *sendStr, bool packFlag, bool printLn)
{
	uint16_t t, len;
	if (packFlag)
		Usart_Pack(sendStr, USART1_TX_BUF);
	else
		memcpy(USART1_TX_BUF, sendStr, strlen((const char *)sendStr) + 1);
	len = strlen((const char *)USART1_TX_BUF);
	if (printLn)
	{
		USART1_TX_BUF[len] = '\n';
		len++;
		USART1_TX_BUF[len] = '\0';
	}
	for (t = 0; t < len; t++)
	{
		USART1->DR = USART1_TX_BUF[t];
		while ((USART1->SR & 0X40) == 0)
			; //等待发送结束
	}
}
#endif

#if EN_USART2_RX //如果使能了接收
//串口2中断服务程序
//注意,读取USARTx->SR能避免莫名其妙的错误
char USART2_RX_BUF[USART_REC_LEN] = {'\0'}; //接收缓冲,最大USART_REC_LEN个字节.
u8 USART2_TX_BUF[USART_REC_LEN];			//发送缓冲,最大USART2_MAX_SEND_LEN字节
//接收状态
u8 waitACK = 0; //防止8266 回应OK 遮盖了 UDP_ACK数据表
//bit15，	接收完成标志
//bit14，	接收到0x0d
//bit13~0，	接收到的有效字节数目
u16 USART2_RX_STA = 0; //接收状态标记
u32 Usart2_Send_Cnt = 0;
u32 Usart2_Rec_Cnt = 0;
s16 Usart2_Rx_Len = 0;
u32 Usart2_Rec_Ing = 0;
bool startTrans2Flag = false; //接收开启标记
void Usart2_init(u32 bound)
{
	//GPIO端口设置
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE); //使能USART2，GPIOA时钟
	USART_DeInit(USART2);
	//USART2_TX   GPIOA.2
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2; //PA.2
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP; //复用推挽输出
	GPIO_Init(GPIOA, &GPIO_InitStructure);			//初始化GPIOA.2

	//USART2_RX	  GPIOA.3初始化
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;			  //PA3
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING; //浮空输入
	GPIO_Init(GPIOA, &GPIO_InitStructure);				  //初始化GPIOA.3

	//Usart2 NVIC 配置
	NVIC_InitStructure.NVIC_IRQChannel = USART2_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 3; //抢占优先级3
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;		  //子优先级3
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;			  //IRQ通道使能
	NVIC_Init(&NVIC_InitStructure);							  //根据指定的参数初始化VIC寄存器

	//USART 初始化设置

	USART_InitStructure.USART_BaudRate = bound;										//串口波特率
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;						//字长为8位数据格式
	USART_InitStructure.USART_StopBits = USART_StopBits_1;							//一个停止位
	USART_InitStructure.USART_Parity = USART_Parity_No;								//无奇偶校验位
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None; //无硬件数据流控制
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;					//收发模式

	USART_Init(USART2, &USART_InitStructure);	   //初始化串口2
	USART_ITConfig(USART2, USART_IT_RXNE, ENABLE); //开启串口接受中断
	USART_Cmd(USART2, ENABLE);					   //使能串口2
}

void USART2_IRQHandler(void) //串口2中断服务程序
{
	u8 Res;
#if SYSTEM_SUPPORT_OS //如果SYSTEM_SUPPORT_OS为真，则需要支持OS.
	OSIntEnter();
#endif
	if (USART_GetITStatus(USART2, USART_IT_RXNE) != RESET) //接收中断(接收到的数据必须是0x0d 0x0a结尾)
	{
		Res = USART_ReceiveData(USART2); //(USART2->DR);	//读取接收到的数据

		if ((USART2_RX_STA & 0x8000) == 0) //接收未完成
		{
			if (Res == '\n')
			{
				// "\n\r"或"\n"均为结束符
				if((USART2_RX_STA & 0X3FFF) == 1)
					USART2_RX_STA = 0;
				else if (USART2_RX_BUF[(USART2_RX_STA & 0X3FFF) - 1] == '\r')
				{
					USART2_RX_BUF[(USART2_RX_STA & 0X3FFF) - 1] = '\0';
					USART2_RX_STA--;
					USART2_RX_STA |= 0x8000;
				}
				else{
					USART2_RX_BUF[USART2_RX_STA & 0X3FFF] = '\0';
					USART2_RX_STA |= 0x8000;
				}
			}
			else
			{
				USART2_RX_BUF[USART2_RX_STA & 0X3FFF] = Res;
				USART2_RX_STA++;
				//USART2_RX_BUF[USART2_RX_STA&0X3FFF]='\0';
				if (USART2_RX_STA > (USART_REC_LEN - 1))
				{
					USART2_RX_STA = 0;
				} //接收数据错误,重新开始接收
			}
		}
	}
#if SYSTEM_SUPPORT_OS //如果SYSTEM_SUPPORT_OS为真，则需要支持OS.
	OSIntExit();
#endif
}

void Usart2_test()
{
	uint16_t times = 0;
	while (1)
	{
		if (USART2_RX_STA & 0x8000)
		{
			printf("\r\n您发送的消息为:\r\n");
			Usart2_Send((u8 *)USART2_RX_BUF, false, true);
			printf("\r\n"); //插入换行
			USART2_RX_STA = 0;
		}
		else
		{
			times++;
			if (times % 5000 == 0)
			{
				printf("\r\nALIENTEK MiniSTM32开发板 串口实验\r\n");
				printf("正点原子@ALIENTEK\r\n\r\n\r\n");
			}
			if (times % 200 == 0)
				printf("请输入数据,以回车键结束\r\n");
			//			if(times%30==0)LED2=!LED2;//闪烁LED,提示系统正在运行.
			delay_ms(10);
		}
	}
}

void Usart2_Send(u8 *sendStr, bool packFlag, bool printLn)
{
//	uint16_t t, len;
////	if (packFlag)
////		Usart_Pack(sendStr, USART2_TX_BUF);
////	else
////		memcpy(USART2_TX_BUF, sendStr, strlen((const char *)sendStr) + 1);
//	len = strlen((const char *)USART2_TX_BUF);
//	if (printLn)
//	{
//		USART2_TX_BUF[len] = '\n';
//		len++;
//		USART2_TX_BUF[len] = '\0';
//	}
//	Usart2_Send_Cnt += len;
//	for (t = 0; t < len; t++)
//	{
//		USART2->DR = USART2_TX_BUF[t];
//		while ((USART2->SR & 0X40) == 0)
//			; //等待发送结束
//	}
		uint16_t t, len;
 	len = strlen(sendStr);
	for(t=0;t<len;t++)
	{
		USART2->DR=sendStr[t];
		while((USART2->SR&0X40)==0);//等待发送结束
	}
	if(printLn)
	{
		USART2->DR=(u8)0x0D;
		while((USART2->SR&0X40)==0);
		USART2->DR=(u8)0x0A;
		while((USART2->SR&0X40)==0);//发送换行
	}
}
#endif
